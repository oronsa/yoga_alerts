

package com.oronsason.yoga_alerts;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

public class SignIn extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "EmailPassword";
    private EditText mEmailField;
    private EditText mPasswordField;
    private FirebaseAuth mAuth;
    private FirebaseUser mFirebaseUser;
    private String urlPath ="https://yogaserverapp.herokuapp.com";
    private String authBase ="/api/authentication";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signin_main);
        // Views
        mEmailField = (EditText) findViewById(R.id.field_email);
        mPasswordField = (EditText) findViewById(R.id.field_password);
        mAuth = FirebaseAuth.getInstance();
        mFirebaseUser =mAuth.getCurrentUser();
    }
    private void signIn(final String email, final String password)  {
        Log.d(TAG, "signIn:" + email);
        if (!validateForm()) {
            return;
        }

        showProgressDialog();

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());

                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithEmail:failed", task.getException());
                            Toast.makeText(SignIn.this, R.string.auth_failed,
                                    Toast.LENGTH_SHORT).show();
                            hideProgressDialog();
                            return;
                        }
                        hideProgressDialog();
                        //in case the password is change from firebase
                        JSONObject data  = new JSONObject();
                        try {
                            data.put("email",email);
                            data.put("password",password);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }//in case we change the password outside the system - forgot password
                        if (data.length() > 0) {
                            new HttpRequest.sendDataToServer(SignIn.this).execute(urlPath+authBase+"/updatePassword",String.valueOf(data));
                        }
                        //in case the token is change and we need to update in our Database
                        data  = new JSONObject();
                        try {
                            data.put("email",email);
                            data.put("token",FirebaseInstanceId.getInstance().getToken());
                            Log.i("data", String.valueOf(data));
                            new HttpRequest.sendDataToServer(SignIn.this).execute(urlPath+authBase+"/updateTokenSignIn",String.valueOf(data));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        FirebaseMessaging.getInstance().subscribeToTopic("default");
                        startActivity(new Intent(SignIn.this,MainActivity.class));
                    }
                });
    }
    //check if the fields not empty and valid!
    private boolean validateForm() {
        boolean valid = true;

        String email = mEmailField.getText().toString();
        if (TextUtils.isEmpty(email)) {
            mEmailField.setError("Required.");
            valid = false;
        } else {
            mEmailField.setError(null);
        }

        String password = mPasswordField.getText().toString();
        if (TextUtils.isEmpty(password)) {
            mPasswordField.setError("Required.");
            valid = false;
        } else {
            mPasswordField.setError(null);
        }
        return valid;
    }
    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.email_sign_in_button)
            signIn(mEmailField.getText().toString(), mPasswordField.getText().toString());
        else if(v.getId() == R.id.email_create_account_button)
            startActivity(new Intent(this,SignUp.class));
        else if(v.getId() == R.id.forgot_pass_click){
            startActivity(new Intent(this,ForgotPassword.class));
        }
    }
    @Override
    public void onBackPressed() {
            moveTaskToBack(true);
    }
}

