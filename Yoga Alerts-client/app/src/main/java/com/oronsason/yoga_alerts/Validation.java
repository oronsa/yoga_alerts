package com.oronsason.yoga_alerts;


import android.util.Patterns;

import java.util.regex.Pattern;

class Validation {
     boolean isValidEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }
     boolean isValidDate(String email) {
        String regex = "^([0-2][0-9]||3[0-1])/(0[0-9]||1[0-2])/([0-9][0-9])?[0-9][0-9]$";
        return email.matches(regex);
    }
}
