package com.oronsason.yoga_alerts;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MessageBlog extends AppCompatActivity {
    ListView listView;
    CustomBlogAdapter adapter;
    DataBaseHelper dataBaseHelper;
    ImageView imageView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.blog_list);

        imageView = (ImageView)findViewById(R.id.removeAll);
        listView = (ListView)findViewById(R.id.blog_list);
        dataBaseHelper = new DataBaseHelper(this);
        Cursor cursor = dataBaseHelper.getAllDataBlog();
        if(cursor == null) {
            Toast.makeText(this,"התרחשה שגיאה, נסה שוב מאוחר יותר",Toast.LENGTH_LONG).show();
        }else{
            //if there is no results from table
            if (cursor.getCount() == 0) {
                Toast.makeText(this,"נכון לעכשיו, אין הודעות להציג בדף זה, נסה לבדוק מאוחר יותר, תודה!",Toast.LENGTH_LONG).show();
            }
            adapter = new CustomBlogAdapter(this, cursor);
            listView.setAdapter(adapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {

                    TextView blog_body = (TextView) view.findViewById(R.id.blog_body);
                    TextView blog_title = (TextView) view.findViewById(R.id.blog_title);
                    TextView blog_date = (TextView) view.findViewById(R.id.blog_date);
                    blog_title.setTypeface(null, Typeface.NORMAL);
                    blog_date.setTypeface(null, Typeface.NORMAL);

                    Cursor cursor = dataBaseHelper.bodyQueryBlog(blog_body.getText().toString());
                    int readIndex = cursor.getColumnIndex("read");
                    while (cursor.moveToNext()) {
                        if (cursor.getString(readIndex).equals("0")) {
                            dataBaseHelper.updateMessageStatus(blog_body.getText().toString(), "1");
                            dataBaseHelper.close();
                        }
                    }
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MessageBlog.this);
                    final LayoutInflater inflater = MessageBlog.this.getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.message_content, null);
                    dialogBuilder.setView(dialogView);
                    final AlertDialog alertDialog = dialogBuilder.create();
                    alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    alertDialog.show();
                    ImageView remove = (ImageView) dialogView.findViewById(R.id.remove_image);
                    TextView title = (TextView) dialogView.findViewById(R.id.show_title);
                    TextView body = (TextView) dialogView.findViewById(R.id.show_body);
                    TextView date = (TextView) dialogView.findViewById(R.id.show_date);
                    TextView remove_text = (TextView) dialogView.findViewById(R.id.delete_note);
                    date.setText(getBaseContext().getString(R.string.date) + " " + blog_date.getText().toString());
                    title.setText(getBaseContext().getString(R.string.title) + " " + blog_title.getText().toString());
                    body.setText(blog_body.getText().toString());
                    remove_text.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dataBaseHelper.deleteDataBlog(((TextView) view.findViewById(R.id.blog_body)).getText().toString());
                            dataBaseHelper.close();
                            alertDialog.dismiss();
                            finish();
                            startActivity(getIntent());
                        }
                    });
                    remove.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dataBaseHelper.deleteDataBlog(((TextView) view.findViewById(R.id.blog_body)).getText().toString());
                            dataBaseHelper.close();
                            alertDialog.dismiss();
                            finish();
                            startActivity(getIntent());
                        }
                    });
                }
            });
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AlertDialog.Builder(MessageBlog.this)
                            .setMessage("האם אתה בטוח שברצונך למחוק את כל ההודעות?")
                            .setCancelable(true)
                            .setPositiveButton("כן", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dataBaseHelper.deleteAllBlog();
                                    dataBaseHelper.close();
                                    finish();
                                    startActivity(getIntent());
                                }
                            })
                            .setNegativeButton("לא",  new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            })
                            .show();

                }

            });
        }
    }
}

