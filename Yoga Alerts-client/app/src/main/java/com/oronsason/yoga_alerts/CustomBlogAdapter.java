package com.oronsason.yoga_alerts;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

// this class defines  a Favorites list using Cursor that includes title and images

class CustomBlogAdapter extends CursorAdapter {

    CustomBlogAdapter(Context context, Cursor cursor) {
        super(context,cursor,0);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        TextView title = (TextView) view.findViewById(R.id.blog_title);
        title.setText(cursor.getString(1));

        TextView body = (TextView) view.findViewById(R.id.blog_body);
        body.setText(cursor.getString(2));

        TextView date = (TextView) view.findViewById(R.id.blog_date);
        date.setText(cursor.getString(3));

        String number = cursor.getString(4);
        if (number.equals("1")) {
            title.setTypeface(null, Typeface.NORMAL);
            body.setTypeface(null, Typeface.NORMAL);
            date.setTypeface(null, Typeface.NORMAL);
        }else{
            title.setTypeface(null, Typeface.BOLD);
            body.setTypeface(null, Typeface.BOLD);
            date.setTypeface(null, Typeface.BOLD);
        }
    }
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.blog_items, parent, false);
    }
}