package com.oronsason.yoga_alerts;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Random;

public class AboutAs extends AppCompatActivity {

    ArrayList<Bitmap> bitmapList;
    ImageView imageView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_studio_page);
        imageView = (ImageView)findViewById(R.id.imageAbout);
        Random rn = new Random();
        int range = 4 + 1;
        int randomNum = rn.nextInt(range);
        Resources res = getResources();
        String mDrawableName = "studio_"+randomNum;
        int resID = res.getIdentifier(mDrawableName , "drawable", getPackageName());
        imageView.setImageResource(resID);
    }


}
