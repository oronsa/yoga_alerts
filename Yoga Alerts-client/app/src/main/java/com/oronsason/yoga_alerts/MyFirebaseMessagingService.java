package com.oronsason.yoga_alerts;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        // TODO(developer): Handle FCM messages here.
        sendNotification(remoteMessage.getData().get("title"),remoteMessage.getData().get("body"),
                remoteMessage.getData().get("url"),remoteMessage.getData().get("flag"),remoteMessage.getData().get("image"));
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

    }

    private void sendNotification(final String messageTitle, final String messageBody, String url, final String flag, final String image) {
        Date currDate = new Date();
        final String fDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(currDate);

        if(flag.equals("blogFlag")){
            DataBaseHelper databaseHelper = new DataBaseHelper(this);
            databaseHelper.insertDataBlog(messageTitle,messageBody,fDate,"0");
            databaseHelper.close();
        }
        else if(image.equals("")) {
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
            Intent intent;
            if (flag.equals("payment")) {
                intent = new Intent(this, PaymentActivity.class);
                intent.putExtra("title", messageTitle);
                intent.putExtra("amount", messageBody);
            }
            else if(!url.equals("")){
                intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(url));

            } else{
                intent = new Intent(this, AlertsPage.class);
                intent.putExtra("title", messageTitle);
                intent.putExtra("body", messageBody);
                intent.putExtra("date", fDate);
                if(flag.equals("contact"))
                    intent.putExtra("contact","yes");
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            int alert_id = NotificationID.getID();
            PendingIntent pendingIntent = PendingIntent.getActivity(this,alert_id, intent, PendingIntent.FLAG_UPDATE_CURRENT);

            long[] pattern = {500, 500, 500, 500, 500};

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


            notificationBuilder.setSmallIcon(R.mipmap.bell_status_bar)
                    .setContentTitle(messageTitle)
                    .setContentText(messageBody)
                    .setAutoCancel(true)
                    .setVibrate(pattern)
                    .setColor(Color.GREEN)
                    .setLights(Color.GREEN, 1, 1)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(NotificationID.getID(), notificationBuilder.build());
        }else {

            ImageRequest imageRequest = new ImageRequest(image, new Response.Listener<Bitmap>() {
                @Override
                public void onResponse(Bitmap response) {
                    Intent intent1 = new Intent(getBaseContext(), AlertsPage.class);
                    intent1.putExtra("title", messageTitle);
                    intent1.putExtra("body", messageBody);
                    intent1.putExtra("date", fDate);
                    intent1.putExtra("image", image);
                    if(flag.equals("contact"))
                        intent1.putExtra("contact","yes");
                    if(flag.equals("birthday"))
                        intent1.putExtra("birthday","yes");
                    intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    int alert_id = NotificationID.getID();
                    PendingIntent pendingIntent = PendingIntent.getActivity(getBaseContext(),alert_id, intent1, PendingIntent.FLAG_UPDATE_CURRENT);

                    long[] pattern = {500, 500, 500, 500, 500};

                    Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getBaseContext())
                            .setSmallIcon(R.mipmap.bell_status_bar)
                            .setContentTitle(messageTitle)
                            .setContentText(messageBody)
                            .setAutoCancel(true)
                            .setVibrate(pattern)
                            .setColor(Color.GREEN)
                            .setLights(Color.GREEN, 1, 1)
                            .setSound(defaultSoundUri)
                            .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(response))
                            .setContentIntent(pendingIntent);

                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.notify(alert_id, notificationBuilder.build());
                }
            }, 0, 0, null, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            MySingleton.getInstance(this).addToRequestQueue(imageRequest);
        }

    }
}