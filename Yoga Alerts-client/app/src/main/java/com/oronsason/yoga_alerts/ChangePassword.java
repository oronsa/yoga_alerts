package com.oronsason.yoga_alerts;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class ChangePassword  extends AppCompatActivity {

    private String urlPath ="https://yogaserverapp.herokuapp.com";
    private String authBase ="/api/authentication";
    EditText et_curr_pass,et_new_pass,et_con_new_pass;
    String currPassword,newPassword,conNewPassword;
    private FirebaseUser user;
    Validation validation;
    FirebaseAuth auth;
    private String TAG ="DETAILS NOTE: ";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_password);
        auth = FirebaseAuth.getInstance();
        et_curr_pass  = (EditText)findViewById(R.id.et_curr_pass);
        et_new_pass = (EditText)findViewById(R.id.et_new_pass);
        et_con_new_pass = (EditText)findViewById(R.id.et_con_new_pass);
        user = FirebaseAuth.getInstance().getCurrentUser();
        validation = new Validation();

    }
    public void onSavePassword(View view){
        new AlertDialog.Builder(this)
                .setMessage("אתה בטוח שברצונך להחליף סיסמא?" +
                        "שים לב : במידה ותלחץ כן תועבר לדף ההתחברות לכניסה חוזרת עם הסיסמא החדשה שהזנת")
                .setCancelable(true)
                .setPositiveButton("כן", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (checkDetails()) {
                            JSONObject data = new JSONObject();
                            try {
                                data.put("email", user.getEmail());
                                data.put("password", et_curr_pass.getText().toString());
                                data.put("newPassword", et_new_pass.getText().toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if (data.length() > 0) {
                                new sendDataToServer(ChangePassword.this).execute(urlPath + authBase + "/editPassword", String.valueOf(data));
                            }
                        }
                    }
                })
                .setNegativeButton("לא", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                        startActivity(getIntent());
                    }
                })
                .show();
    }
    private boolean checkDetails(){
        currPassword = et_curr_pass.getText().toString();
        newPassword = et_new_pass.getText().toString();
        conNewPassword = et_con_new_pass.getText().toString();
        Boolean valid = true;

        if (TextUtils.isEmpty(currPassword)) {
            et_curr_pass.setError("Required.");
            valid = false;
        }
        else if (TextUtils.isEmpty(newPassword)) {
            et_new_pass.setError("Required.");
            valid = false;
        }
        else if (TextUtils.isEmpty(currPassword)) {
            et_con_new_pass.setError("Required.");
            valid = false;
        }

        else if(!newPassword.equals(conNewPassword)){
            et_con_new_pass.setError("Password not much!");
            valid = false;
        }
        else if(newPassword.length()<6){
            et_curr_pass.setError("Min 6 Character,try agin");
            valid = false;
        }
        return valid;
    }
    private void updatePasswordInFirebase(String newPassword) {
        user.updatePassword(newPassword)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "User password updated.");
                            auth.signOut();
                            startActivity(new Intent(ChangePassword.this, SignIn.class));
                        }
                    }
                });
    }
    class sendDataToServer extends AsyncTask<String, String, String> {
        Context context;

        sendDataToServer(Context context) {
            this.context = context;
        }

        ProgressDialog progressDialog;

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(getString(R.string.loading));
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            try {
                JSONObject jsonObject = new JSONObject(result);
                Log.i(TAG,jsonObject.toString());
                String success = jsonObject.getString("success");
                if(success.equals("true")){
                    updatePasswordInFirebase(newPassword);
                }else{
                    Toast.makeText(getBaseContext(),"Error while trying change pass,try again",Toast.LENGTH_LONG).show();
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                return postData(params[0], params[1]);
            } catch (IOException ec) {
                return "Network error !";
            } catch (JSONException ex) {
                return "Data invalid!";
            }
        }

        private String postData(String urlPath, String data) throws IOException, JSONException {
            StringBuilder result = new StringBuilder();
            //create data and send into server
            BufferedWriter bufferedWriter = null;
            BufferedReader bufferedReader = null;
            try {
                URL url = new URL(urlPath);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setReadTimeout(10000);
                urlConnection.setConnectTimeout(10000);
                urlConnection.setRequestMethod("POST");
                urlConnection.setDoOutput(true);    //enable output (body extra)
                urlConnection.setRequestProperty("Content-Type", "application/json");   //set header
                urlConnection.connect();

                //write data into server
                OutputStream outputStream = urlConnection.getOutputStream();
                bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));
                bufferedWriter.write(data);
                bufferedWriter.flush();

                //read response from server
                InputStream inputStream = urlConnection.getInputStream();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    result.append(line).append("\n");
                }
            } finally {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
                if (bufferedWriter != null) {
                    bufferedWriter.close();
                }
            }
            return result.toString();
        }

    }
}
