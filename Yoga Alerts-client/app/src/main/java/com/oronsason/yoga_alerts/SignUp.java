package com.oronsason.yoga_alerts;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class SignUp extends BaseActivity {

    private static final String TAG = "EmailPassword";
    private String urlPath ="https://yogaserverapp.herokuapp.com";
    private String authBase ="/api/authentication";

    Validation validation = new Validation();
    private FirebaseAuth mAuth;
    EditText et_name, et_lastName, et_email,et_password, et_confirmPassword, et_token,et_birthday,et_phone;
    private String name;
    private String lastName;
    private String password;
    private String email;
    private String birthday;
    private String phone;
    private String token;

    Button signUp;
    Calendar myCalendar = Calendar.getInstance();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up);

        signUp = (Button) findViewById(R.id.btn_sign_up);
        et_name = (EditText) findViewById(R.id.et_first_name);
        et_lastName = (EditText) findViewById(R.id.et_last_name);
        et_email = (EditText) findViewById(R.id.et_email);
        et_password = (EditText)findViewById(R.id.et_password);
        et_confirmPassword = (EditText) findViewById(R.id.et_confirm);
        et_token = (EditText) findViewById(R.id.et_token);
        et_birthday = (EditText) findViewById(R.id.et_birthday);
        et_phone = (EditText) findViewById(R.id.et_phone);
        mAuth = FirebaseAuth.getInstance();

        //button listener for sign up action
        signUp.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check if the details in form are valid
                if(validateForm()){
                    try {
                        checkToken();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        et_birthday.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                new DatePickerDialog(SignUp.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }
    private boolean validateForm() {

        name = et_name.getText().toString();
        lastName = et_lastName.getText().toString();
        password =et_password.getText().toString();
        email = et_email.getText().toString();
        phone = et_phone.getText().toString();
        String confirmPassword = et_confirmPassword.getText().toString();
        token = et_token.getText().toString();
        birthday = et_birthday.getText().toString();

        boolean valid = true;

        if (TextUtils.isEmpty(name)) {
            et_name.setError("שדה חובה");
            valid = false;
        } else if (TextUtils.isEmpty(lastName)) {
            et_lastName.setError("שדה חובה");
            valid = false;
        }  else if (TextUtils.isEmpty(email)) {
            et_email.setError("שדה חובה");
            valid = false;
        }else if(!validation.isValidEmail(email)){
            et_email.setError("המייל אינו תקין, נסה שוב");
            valid = false;
        }
        else if(TextUtils.isEmpty(phone)){
            et_phone.setError("שדה חובה");
            valid = false;
        } else if (TextUtils.isEmpty(password)) {
            et_password.setError("שדה חובה");
            valid = false;
        } else if (TextUtils.isEmpty(confirmPassword)){
            et_confirmPassword.setError("שדה חובה");
            valid = false;
        } else if (TextUtils.isEmpty(token)){
            et_token.setError("שדה חובה");
            valid = false;
        } else if (TextUtils.isEmpty(birthday)){
            et_birthday.setError("שדה חובה");
            valid = false;
        }else if(!password.equals(confirmPassword)){
            et_password.setError("שדה סיסמא אינו תואם לאישור סיסמא");
            et_confirmPassword.setError("שדה אישור סיסמא אינו תואם לסיסמא");
            valid = false;
        }
        return valid;
    }
    private void checkToken() throws JSONException {
        JSONObject data = new JSONObject();
        try {
            data.put("email", email);
            data.put("token", token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (data.length() > 0) {
            new sendDataToServer(SignUp.this).execute(urlPath + authBase + "/checkToken", String.valueOf(data));
        }
    }

    private void updateLabel() {

        String myFormat = "dd/MM/yy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat,Locale.US);

        et_birthday.setText(sdf.format(myCalendar.getTime()));
    }
    private void saveInSharedPreferences()
    {
        SharedPreferences.Editor editor = getSharedPreferences("tokenPreferences", MODE_PRIVATE).edit();
        editor.putString("token",FirebaseInstanceId.getInstance().getToken());
        editor.apply();
    }
    class sendDataToServer extends AsyncTask<String, String, String> {
        Context context;

        sendDataToServer(Context context) {
            this.context = context;
        }

        ProgressDialog progressDialog;

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(getString(R.string.loading));
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            try {
                JSONObject object = new JSONObject(result);
                String success = object.getString("success");
                if(success.equals("wrongToken")){
                    et_token.setError("בדוק את המפתח שנשלח אליך למייל");
                }else {
                    showProgressDialog();
                    //create user with email and password
                    Log.d("this email and pass:", email + password);
                    mAuth.createUserWithEmailAndPassword(email, password)
                            .addOnCompleteListener(SignUp.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    Log.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());

                                    //if from any reason firebase signUp is fails!
                                    if (!task.isSuccessful()) {
                                        Toast.makeText(getBaseContext(), R.string.auth_failed, Toast.LENGTH_SHORT).show();
                                        hideProgressDialog();
                                        return;
                                    }
                                    hideProgressDialog();
                                    saveInSharedPreferences();

                                    //set the data that send to mongoDB
                                    JSONObject data = new JSONObject();
                                    try {
                                        data.put("name", name);
                                        data.put("lastName", lastName);
                                        data.put("email", email);
                                        data.put("password", password);
                                        data.put("birthday", birthday);
                                        data.put("phone", phone);
                                        data.put("firebaseToken", FirebaseInstanceId.getInstance().getToken());
                                        data.put("uid", mAuth.getCurrentUser().getUid());
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    if (data.length() > 0) {
                                        new HttpRequest.sendDataToServer(SignUp.this).execute(urlPath + authBase + "/signUp", String.valueOf(data));
                                    }
                                    //all the users will have to subscribe on this topic!
                                    FirebaseMessaging.getInstance().subscribeToTopic("default");
                                    //save his token in SharedPreferences
                                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putString("token",FirebaseInstanceId.getInstance().getToken());
                                    editor.apply();

                                    startActivity(new Intent(SignUp.this, MainActivity.class));
                                }
                            });
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                return postData(params[0], params[1]);
            } catch (IOException ec) {
                return "Network error !";
            } catch (JSONException ex) {
                return "Data invalid!";
            }
        }

        private String postData(String urlPath, String data) throws IOException, JSONException {
            StringBuilder result = new StringBuilder();
            //create data and send into server
            BufferedWriter bufferedWriter = null;
            BufferedReader bufferedReader = null;
            try {
                URL url = new URL(urlPath);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setReadTimeout(10000);
                urlConnection.setConnectTimeout(10000);
                urlConnection.setRequestMethod("POST");
                urlConnection.setDoOutput(true);    //enable output (body extra)
                urlConnection.setRequestProperty("Content-Type", "application/json");   //set header
                urlConnection.connect();

                //write data into server
                OutputStream outputStream = urlConnection.getOutputStream();
                bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));
                bufferedWriter.write(data);
                bufferedWriter.flush();

                //read response from server
                InputStream inputStream = urlConnection.getInputStream();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    result.append(line).append("\n");
                }
            } finally {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
                if (bufferedWriter != null) {
                    bufferedWriter.close();
                }
            }
            return result.toString();
        }

    }
}
