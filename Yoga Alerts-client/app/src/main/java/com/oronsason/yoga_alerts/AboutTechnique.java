package com.oronsason.yoga_alerts;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import java.util.Random;


public class AboutTechnique extends AppCompatActivity {
    ImageView imageView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_technique_page);
        imageView = (ImageView) findViewById(R.id.imageTechnique);
        Random rn = new Random();
        int range = 3 + 1;
        int randomNum = rn.nextInt(range);
        Resources res = getResources();
        String mDrawableName = "technique_" + randomNum;
        int resID = res.getIdentifier(mDrawableName, "drawable", getPackageName());
        imageView.setImageResource(resID);

    }

}
