package com.oronsason.yoga_alerts;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


public class AlertsPage extends AppCompatActivity {

    TextView title,body,date;
    ImageView imageView;
    Button contactBtn;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.message_page);
        title =  (TextView) findViewById(R.id.title);
        body =  (TextView) findViewById(R.id.body);
        date =  (TextView) findViewById(R.id.date);
        imageView = (ImageView)findViewById(R.id.imageAlert);
        contactBtn = (Button)findViewById(R.id.contact);
        String title_str = getIntent().getExtras().getString("title","");
        String body_str = getIntent().getExtras().getString("body","");
        String date_str = getIntent().getExtras().getString("date","");
        String urlImage = getIntent().getExtras().getString("image","");
        String contact = getIntent().getExtras().getString("contact","");
        String birthday = getIntent().getExtras().getString("birthday","");
        if(contact.equals("yes")){
            contactBtn.setVisibility(View.VISIBLE);
        }
        if(birthday.equals("yes")){
            String uri = "@drawable/birthday_photo";

            int imageResource = getResources().getIdentifier(uri, null, getPackageName());

            Drawable res = getResources().getDrawable(imageResource);
            imageView.setImageDrawable(res);
        }
        else if(!urlImage.equals("")) {
            Picasso.with(this)
                    .load(urlImage)
                    .resize(1366,768)
                    .centerInside()
                    .into(imageView);
        }
        title.setText(title_str);
        body.setText(body_str);
        date.setText(date_str);

        contactBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto",getResources().getString(R.string.adminAddress), null));
                startActivity(Intent.createChooser(emailIntent, null));
            }
        });
    }
}
