package com.oronsason.yoga_alerts;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


public class MyDetails extends AppCompatActivity {

    Button edit_btn;
    EditText edit_name,  edit_lastName,  edit_email, edit_birthday,edit_phone;
    private String name,lastName,email,birthday,phone;
    private String urlPath ="https://yogaserverapp.herokuapp.com";
    private String authBase ="/api/authentication";
    private String TAG ="DETAILS NOTE: ";
    private FirebaseUser user;
    private Validation validation;
    Calendar myCalendar = Calendar.getInstance();


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_details);
        validation = new Validation();
        user = FirebaseAuth.getInstance().getCurrentUser();

        user = FirebaseAuth.getInstance().getCurrentUser();
        edit_btn = (Button)findViewById(R.id.edit_btn);
        edit_name = (EditText) findViewById(R.id.edit_name);
        edit_lastName = (EditText) findViewById(R.id.edit_last_name);
        edit_email = (EditText) findViewById(R.id.edit_email);
        edit_birthday = (EditText) findViewById(R.id.edit_birthday);
        edit_phone = (EditText) findViewById(R.id.edit_phone);

        JSONObject data  = new JSONObject();
        try {
            data.put("email",user.getEmail());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (data.length() > 0) {
            new sendDataToServer(MyDetails.this).execute(urlPath + authBase + "/getDetails", String.valueOf(data));
        }
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        edit_birthday.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new DatePickerDialog(MyDetails.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }
    public void onDetailsClick(View view){
        final String oldMail = user.getEmail();
        if(view.getId() == R.id.submit_btn) {
            new AlertDialog.Builder(this)
                    .setMessage("האם אתה בטוח שברצונך לשמור את הנתונים?")
                    .setCancelable(true)
                    .setPositiveButton("כן", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            name = edit_name.getText().toString();
                            lastName = edit_lastName.getText().toString();
                            email = edit_email.getText().toString();
                            birthday = edit_birthday.getText().toString();
                            phone = edit_phone.getText().toString();
                            Boolean check = checkFields();
                            if (check) {
                                if (oldMail!= null) {
                                    if (!oldMail.equals(email)) {
                                        if (validation.isValidEmail(email)) {
                                            //Set a user's email address
                                            updateEmailInFirebase(email);
                                            //Send a user a verification email
                                            sendMessageUpdateEmail();
                                        }
                                    }
                                }
                                JSONObject data = new JSONObject();
                                try {
                                    data.put("name", name);
                                    data.put("lastName", lastName);
                                    data.put("email", email);
                                    data.put("oldMail", oldMail);
                                    data.put("birthday", birthday);
                                    data.put("phone", phone);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                if (data.length() > 0) {
                                    new HttpRequest.sendDataToServer(MyDetails.this).execute(urlPath + authBase + "/updateDetails", String.valueOf(data));
                                }
                            }else{
                                return;
                            }
                            edit_name.setEnabled(false);
                            edit_lastName.setEnabled(false);
                            edit_email.setEnabled(false);
                            edit_birthday.setEnabled(false);
                            edit_phone.setEnabled(false);
                            startActivity(new Intent(MyDetails.this,MainActivity.class));
                        }
                    })
                    .setNegativeButton("לא",  new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                            startActivity(getIntent());
                        }
                    })
                    .show();
        }
        else if(view.getId() == R.id.edit_btn){
            edit_name.requestFocus();
            if(edit_btn.getText().toString().equals(getResources().getString(R.string.edit_details))){
                edit_btn.setText(R.string.cencel);
                edit_name.setEnabled(true);
                edit_lastName.setEnabled(true);
                edit_email.setEnabled(true);
                edit_birthday.setEnabled(true);
                edit_phone.setEnabled(true);
            }else {
                edit_btn.setText(R.string.edit_details);
                edit_name.setEnabled(false);
                edit_lastName.setEnabled(false);
                edit_email.setEnabled(false);
                edit_birthday.setEnabled(false);
                edit_phone.setEnabled(false);
            }
        }

    }
    private void updateLabel() {

        String myFormat = "dd/MM/yy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        edit_birthday.setText(sdf.format(myCalendar.getTime()));
    }
    private void updateEmailInFirebase(String email) {
        user.updateEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "User email address updated.");
                        }
                    }
                });
    }
    private void sendMessageUpdateEmail(){
        user.sendEmailVerification()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "Email sent.");
                        }
                    }
                });
    }
    private boolean checkFields() {

        boolean valid = true;
        if (TextUtils.isEmpty(name)) {
            edit_name.setError("Required.");
            valid = false;
        }
        if (TextUtils.isEmpty(lastName)) {
            edit_lastName.setError("Required.");
            valid = false;
        }
        if (TextUtils.isEmpty(birthday)) {
            edit_birthday.setError("Required.");
            valid = false;
        }
        if (TextUtils.isEmpty(phone)) {
            edit_phone.setError("Required.");
            valid = false;
        }
        else if(!validation.isValidDate(birthday)) {
            edit_birthday.setError("Not Valid, please use format:"+"\n"+" dd/mm/yy.");
            valid = false;
        }
        return valid;
    }
    class sendDataToServer extends AsyncTask<String, String, String> {
        Context context;

        sendDataToServer(Context context) {
            this.context = context;
        }

        ProgressDialog progressDialog;

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(getString(R.string.loading));
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            try {
                Log.d(TAG,result);
                JSONObject jsonObject = new JSONObject(result);
                JSONArray arr = jsonObject.getJSONArray("obj");
                edit_name.setText(arr.getJSONObject(0).getString("name"));
                edit_lastName.setText(arr.getJSONObject(0).getString("lastName"));
                edit_email.setText(arr.getJSONObject(0).getString("email"));
                edit_birthday.setText(arr.getJSONObject(0).getString("birthday"));
                edit_phone.setText(arr.getJSONObject(0).getString("phone"));
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                return postData(params[0], params[1]);
            } catch (IOException ec) {
                return "Network error !";
            } catch (JSONException ex) {
                return "Data invalid!";
            }
        }

        private String postData(String urlPath, String data) throws IOException, JSONException {
            StringBuilder result = new StringBuilder();
            //create data and send into server
            BufferedWriter bufferedWriter = null;
            BufferedReader bufferedReader = null;
            try {
                URL url = new URL(urlPath);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setReadTimeout(10000);
                urlConnection.setConnectTimeout(10000);
                urlConnection.setRequestMethod("POST");
                urlConnection.setDoOutput(true);    //enable output (body extra)
                urlConnection.setRequestProperty("Content-Type", "application/json");   //set header
                urlConnection.connect();

                //write data into server
                OutputStream outputStream = urlConnection.getOutputStream();
                bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));
                bufferedWriter.write(data);
                bufferedWriter.flush();

                //read response from server
                InputStream inputStream = urlConnection.getInputStream();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    result.append(line).append("\n");
                }
            } finally {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
                if (bufferedWriter != null) {
                    bufferedWriter.close();
                }
            }
            return result.toString();
        }

    }
}
