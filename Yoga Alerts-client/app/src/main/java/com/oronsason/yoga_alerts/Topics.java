package com.oronsason.yoga_alerts;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;


public class Topics extends AppCompatActivity {
    ListView lv;
    ArrayList<Item> itemList;
    MyAdapter myAdapter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.topics);
        lv = (ListView) findViewById(R.id.list_view);
        String urlPath = "https://yogaserverapp.herokuapp.com";
        String authBase = "/api/authentication";
        new Topics.getDataFromServer(Topics.this).execute(urlPath + authBase + "/importTopics");
    }

    class getDataFromServer extends AsyncTask<String, String, String> {
        Context context;

        getDataFromServer(Context context) {
            this.context = context;
        }

        ProgressDialog progressDialog;

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(getString(R.string.loading));
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            //TODO: GET THE RETURNING DATA
            try {
                String TAG = "TOPICS NOTE: ";
                Log.d(TAG,result);
                JSONObject jsonObject = new JSONObject(result);
                JSONArray arr = jsonObject.getJSONArray("obj");
                Log.d(TAG, String.valueOf(arr.length()));
                itemList = new ArrayList<>();
                for(int i =0 ; i < arr.length() ; i++) {

                    itemList.add(new Item(arr.getJSONObject(i).getString("nameHebrew")+"-"+arr.getJSONObject(i).getString("topicTitle")));
                }
                myAdapter = new MyAdapter(itemList,Topics.this);
                lv.setAdapter(myAdapter);
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                return postData(params[0]);
            } catch (IOException ec) {
                return "Network error !";
            } catch (JSONException ex) {
                return "Data invalid!";
            }
        }

        private String postData(String urlPath) throws IOException, JSONException {
            StringBuilder result = new StringBuilder();
            //create data and send into server
            BufferedWriter bufferedWriter = null;
            BufferedReader bufferedReader = null;
            try {
                URL url = new URL(urlPath);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setReadTimeout(10000);
                urlConnection.setConnectTimeout(10000);
                urlConnection.setRequestMethod("GET");
                urlConnection.setDoOutput(true);    //enable output (body extra)
                urlConnection.setRequestProperty("Content-Type", "application/json");   //set header
                urlConnection.connect();

                //read response from server
                InputStream inputStream = urlConnection.getInputStream();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    result.append(line).append("\n");
                }
            } finally {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
                if (bufferedWriter != null) {
                    bufferedWriter.close();
                }
            }
            return result.toString();
        }

    }
}


