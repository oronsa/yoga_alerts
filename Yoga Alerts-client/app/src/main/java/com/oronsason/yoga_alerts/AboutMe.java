package com.oronsason.yoga_alerts;


import android.content.res.Resources;

import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import java.util.Random;


public class AboutMe  extends AppCompatActivity {

    ImageView imageView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_teacher_page);
        imageView = (ImageView) findViewById(R.id.imageAboutMe);
        Random rn = new Random();
        int range = 3 + 1;
        int randomNum = rn.nextInt(range);
        Resources res = getResources();
        String mDrawableName = "me_"+randomNum;
        int resID = res.getIdentifier(mDrawableName , "drawable", getPackageName());
        imageView.setImageResource(resID);

    }

}
