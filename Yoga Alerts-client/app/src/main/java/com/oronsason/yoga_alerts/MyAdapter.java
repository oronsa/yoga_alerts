package com.oronsason.yoga_alerts;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.google.firebase.messaging.FirebaseMessaging;

import java.util.List;

class Item{
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

     Boolean getSelected() {
         return false;
    }

    private String name;

    Item(String name) {
        super();
        this.name = name;
    }

}
 class MyAdapter extends ArrayAdapter<Item> {

    private List<Item> itemList;
    private Context context;

     MyAdapter(List<Item> itemList,Context context) {
        super(context,R.layout.topic_item,itemList);
        this.itemList = itemList;
        this.context = context;
    }
    private static class ItemHolder{
         TextView itemName;
         CheckBox checkBox;
    }
    @NonNull
    public View getView(int position, View convertView, @NonNull ViewGroup group) {
        View view ;
        final ItemHolder holder = new ItemHolder();

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.topic_item,group,false);

            holder.itemName = (TextView) view.findViewById(R.id.name);
            holder.checkBox = (CheckBox) view.findViewById(R.id.check_box);

           Item i = itemList.get(position);
           holder.itemName.setText(i.getName());
           holder.checkBox.setChecked(i.getSelected());
           holder.checkBox.setTag(i);
           SharedPreferences settings = context.getSharedPreferences("data",Context.MODE_PRIVATE);
           boolean Checked = settings.getBoolean(holder.itemName.getText().toString(), false);
           holder.checkBox.setChecked(Checked);
           holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

             @Override
             public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                 String topic;
                 if(holder.checkBox.isChecked()){

                     SharedPreferences settings = context.getSharedPreferences("data", Context.MODE_PRIVATE);
                     topic=holder.itemName.getText().toString();
                     String arr[] = topic.split("-");
                     FirebaseMessaging.getInstance().subscribeToTopic(arr[1]);
                     settings.edit().putBoolean(holder.itemName.getText().toString(),true).apply();
                 }
                 else {
                     SharedPreferences settings = context.getSharedPreferences("data", Context.MODE_PRIVATE);
                     topic=holder.itemName.getText().toString();
                     String arr[] = topic.split("-");
                     FirebaseMessaging.getInstance().unsubscribeFromTopic(arr[1]);
                     settings.edit().putBoolean(holder.itemName.getText().toString(),false).apply();
                 }
             }
         });
        return view;
    }
}
