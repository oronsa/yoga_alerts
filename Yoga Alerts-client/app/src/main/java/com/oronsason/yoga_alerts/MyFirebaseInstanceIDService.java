package com.oronsason.yoga_alerts;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.json.JSONException;
import org.json.JSONObject;


public  class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = "MyFirebaseInsIDService";

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "New token: " + refreshedToken);
        //you can save token into third party server to do what you want
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String newToken) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        boolean previouslyStarted = prefs.getBoolean(getString(R.string.pref_previously_started), false);
        if (!previouslyStarted) {
            SharedPreferences.Editor edit = prefs.edit();
            edit.putBoolean(getString(R.string.pref_previously_started), Boolean.TRUE);
            edit.apply();
        }
        //we have token already for this device
        else {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            String oldToken = preferences.getString("token", "");
            JSONObject data = new JSONObject();
            try {
                data.put("oldToken", oldToken);
                data.put("newToken", newToken);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (data.length() > 0) {
                String authBase = "/api/authentication";
                String urlPath = "http://   192.168.43.120:1000";
                new HttpRequest.sendDataToServer(MyFirebaseInstanceIDService.this).execute(urlPath + authBase + "/updateToken", String.valueOf(data));
            }
        }
    }
}