package com.oronsason.yoga_alerts;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.Random;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private FirebaseAuth mFirebaseAuth;
    private String mUsername;
    private String userPhotoUrl;
    public static final String ANONYMOUS = "anonymous";
    TextView randomSentence;
    Button whats_new;
    ImageView imageView;
    DataBaseHelper dataBaseHelper;
    SwipeRefreshLayout mySwipeRefreshLayout;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = (ImageView) findViewById(R.id.imageMain);
        Random rn = new Random();
        int range = 10+ + 1;
        int randomNum = rn.nextInt(range);
        Resources res = getResources();
        String mDrawableName = "main_"+randomNum;
        int resID = res.getIdentifier(mDrawableName , "drawable", getPackageName());
        imageView.setImageResource(resID);

        SharedPreferences settings = getSharedPreferences("prefs", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("firstRun", false);
        editor.apply();

        whats_new = (Button)findViewById(R.id.new_message);
        mySwipeRefreshLayout = (SwipeRefreshLayout)this.findViewById(R.id.swipeContainer);
        randomSentence = (TextView)findViewById(R.id.randomSentence);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mFirebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mUsername = ANONYMOUS;
        if (mFirebaseUser == null) {
            // Not signed in, launch the Sign In activity
            startActivity(new Intent(this, SignIn.class));
            finish();
        } else {
            mUsername = mFirebaseUser.getEmail();
            String[] sentence = new String[10];

            for(int i = 0; i < 10 ; i++){
                try {
                    int res_string = R.string.class.getField("sentence_"+i).getInt(null);
                    sentence[i] = this.getResources().getString(res_string);
                } catch (NoSuchFieldException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }

            int rnd = new Random().nextInt(sentence.length);
            randomSentence.setText(sentence[rnd]);
            whats_new.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(MainActivity.this,MessageBlog.class);
                    startActivity(i);
                }
            });
            if (mFirebaseUser.getPhotoUrl() != null) {
                userPhotoUrl = mFirebaseUser.getPhotoUrl().toString();
            }
            Log.d("firebaseToken:", String.valueOf(FirebaseInstanceId.getInstance().getToken()));
        }

        dataBaseHelper = new DataBaseHelper(this);
        Cursor cursor = dataBaseHelper.countUnreadMessage();
        cursor.moveToFirst();
        int count = cursor.getInt(0);
        Log.i("this is my counter", String.valueOf(count));
        whats_new.setText(getResources().getString(R.string.whats_new)+"("+ count +")");
        dataBaseHelper.close();

        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        recreate();                   }
                }
        );
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            moveTaskToBack(true);
        }
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                startActivity(new Intent(this, Topics.class));
                return true;
            case R.id.sign_out:
                mFirebaseAuth.signOut();
                mUsername = ANONYMOUS;
                startActivity(new Intent(this, SignIn.class));
                return true;
            case R.id.my_details:
                startActivity(new Intent(this, MyDetails.class));
                return true;
            case R.id.changePassword:
                startActivity(new Intent(this, ChangePassword.class));
                return true;
            case R.id.nav_contact:
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto",this.getResources().getString(R.string.adminAddress), null));
                this.startActivity(Intent.createChooser(emailIntent, null));
                return true;
            case R.id.nav_about_as:
                startActivity(new Intent(this,AboutAs.class));
                return true;
            case R.id.nav_about_me:
                startActivity(new Intent(this,AboutMe.class));
                return true;
            case R.id.nav_about_technics:
                startActivity(new Intent(this,AboutTechnique.class));
                return true;
            default:
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                return true;
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        //update unread message
        dataBaseHelper = new DataBaseHelper(this);
        Cursor cursor = dataBaseHelper.countUnreadMessage();
        cursor.moveToFirst();
        int count = cursor.getInt(0);
        Log.i("this is my counter", String.valueOf(count));
        whats_new.setText(getResources().getString(R.string.whats_new)+"("+ count +")");
        dataBaseHelper.close();
    }
}
