package com.oronsason.yoga_alerts;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;


public class ForgotPassword extends AppCompatActivity {
    EditText et_recover_mail;
    private String urlPath = "https://yogaserverapp.herokuapp.com";
    private String authBase = "/api/authentication";
    private String TAG = "DETAILS NOTE: ";
    private FirebaseAuth auth;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password);
        et_recover_mail = (EditText) findViewById(R.id.et_recover_email);
        auth = FirebaseAuth.getInstance();
    }

    public void sendRecoverClick(View view) {
        String recoverMailAddress = et_recover_mail.getText().toString();
        if (TextUtils.isEmpty(recoverMailAddress)) {
            et_recover_mail.setError("Required.");
        } else {
            new AlertDialog.Builder(this)
                    .setMessage("אם תלחץ כן, ישלח אליך מייל לשינוי סיסמא, באם ברצונך להמשיך?")
                    .setCancelable(true)
                    .setPositiveButton("כן", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            JSONObject data = new JSONObject();
                            try {
                                data.put("email", et_recover_mail.getText().toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if (data.length() > 0) {
                                new sendDataToServer(ForgotPassword.this).execute(urlPath + authBase + "/checkEmail", String.valueOf(data));
                            }
                        }
                    })
                    .setNegativeButton("לא", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    })
                    .show();
        }
    }

    private void sendPasswordResetEmail(String email) {
        auth.sendPasswordResetEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "Email sent.");
                        }
                    }
                });
    }

    class sendDataToServer extends AsyncTask<String, String, String> {
        Context context;

        sendDataToServer(Context context) {
            this.context = context;
        }

        ProgressDialog progressDialog;

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(getString(R.string.loading));
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            try {
                JSONObject jsonObject = new JSONObject(result);
                Log.i(TAG, jsonObject.toString());
                String success = jsonObject.getString("success");
                if (success.equals("true")) {
                    sendPasswordResetEmail(et_recover_mail.getText().toString());
                    startActivity(new Intent(ForgotPassword.this, SignIn.class));
                } else if (success.equals("false")) {
                    Toast.makeText(getBaseContext(), "בדוק את המייל שהזנת ונסה שנית, תודה!", Toast.LENGTH_LONG).show();
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                return postData(params[0], params[1]);
            } catch (IOException ec) {
                return "Network error !";
            } catch (JSONException ex) {
                return "Data invalid!";
            }
        }

        private String postData(String urlPath, String data) throws IOException, JSONException {
            StringBuilder result = new StringBuilder();
            //create data and send into server
            BufferedWriter bufferedWriter = null;
            BufferedReader bufferedReader = null;
            try {
                URL url = new URL(urlPath);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setReadTimeout(10000);
                urlConnection.setConnectTimeout(10000);
                urlConnection.setRequestMethod("POST");
                urlConnection.setDoOutput(true);    //enable output (body extra)
                urlConnection.setRequestProperty("Content-Type", "application/json");   //set header
                urlConnection.connect();

                //write data into server
                OutputStream outputStream = urlConnection.getOutputStream();
                bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));
                bufferedWriter.write(data);
                bufferedWriter.flush();

                //read response from server
                InputStream inputStream = urlConnection.getInputStream();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    result.append(line).append("\n");
                }
            } finally {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
                if (bufferedWriter != null) {
                    bufferedWriter.close();
                }
            }
            return result.toString();
        }

    }
}
