package com.oronsason.yoga_alerts;


        import android.content.ContentValues;
        import android.content.Context;
        import android.database.Cursor;
        import android.database.sqlite.SQLiteDatabase;
        import android.database.sqlite.SQLiteOpenHelper;

class  DataBaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "blogBase.db";
    private static final String BLOG_TABLE = "favorites_table";
    private static final String ID = "_id";
    private static final String TITLE = "title";
    private static final String BODY = "body";
    private static final String DATE = "date";
    private static final String READ = "read";
    private static final int DATABASE_VERSION  = 1;

    DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + BLOG_TABLE + " (" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT" +
                "," + TITLE + " TEXT," + BODY + " TEXT," + DATE + " TEXT,"+ READ + " TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + BLOG_TABLE);
    }

    boolean insertDataBlog(String title, String body,String date,String read) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TITLE, title);
        values.put(BODY, body);
        values.put(DATE, date);
        values.put(READ, read);
        Long result = db.insert(BLOG_TABLE, null, values);
        db.close();
        return result != -1;
    }

    Cursor getAllDataBlog() {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.rawQuery("select * from " + BLOG_TABLE + " order BY "+ ID + " DESC", null);
    }
    Integer deleteDataBlog(String body) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(BLOG_TABLE, "BODY=?", new String[]{body});
    }
    Cursor bodyQueryBlog(String body) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.rawQuery("SELECT * FROM "+ BLOG_TABLE +" WHERE BODY=?", new String[]{body});
    }
    void deleteAllBlog(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(BLOG_TABLE, null, null);
        db.close();
    }
    void updateMessageStatus(String body,String read){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("UPDATE " + BLOG_TABLE +" SET READ="+ read +" WHERE BODY=?",new String[]{body});
        db.close();
    }
    Cursor countUnreadMessage(){
        SQLiteDatabase db = this.getWritableDatabase();
        return db.rawQuery("select count(*) from " + BLOG_TABLE + " where read=0", null);
    }

}
