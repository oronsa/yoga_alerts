var express = require('express');
var router = require('express').Router();
notification = require('../../utils/notifications');
var client = require('../../utils/clientUtils/clientUtil');
var httpStatus = require('http-status-codes');

//send message by topic
router.post('/sendToTopic', function (request, response) {
    notification.message_to_topic(request.body.data, function (error, result) {
            if (error) {
                client.sendErrorResponse(response, error, httpStatus.INTERNAL_SERVER_ERROR);
            }
            else if (result.success) {
                //TODO: welcome email
                client.sendObjectResponse(response, result);
            }
            else{
                client.sendObjectResponse(response, result);
            }
        }
    );
});

//send message in message page for all users
router.post('/sendToBlog', function (request, response) {
    notification.sendToBlog(request.body.data, function (error, result) {
            if (error) {
                client.sendErrorResponse(response, error, httpStatus.INTERNAL_SERVER_ERROR);
            }
            else
                client.sendObjectResponse(response, result);
        }
    );
});

//send a payment note for client
router.post('/paymentToClient', function (request, response) {
    notification.paymentToClient(request.body.data, function (error, result) {
            if (error) {
                client.sendErrorResponse(response, error, httpStatus.INTERNAL_SERVER_ERROR);
            }
            else
                client.sendObjectResponse(response, result);
        }
    );
});

//send a message to specific client
router.post('/sendToClient', function (request, response) {
    notification.message_to_client(request.body.data, function (error, result) {
            if (error) {
                client.sendErrorResponse(response, error, httpStatus.INTERNAL_SERVER_ERROR);
            }
            else if (result.success) {
                //TODO: welcome email
                client.sendObjectResponse(response, result);
            }
            else{
                client.sendObjectResponse(response, result);
            }
        }
    );
});

//create new autoAlert associate to specific topic
router.post('/createNewAlert', function (request, response) {
    notification.createNewAlert(request.body.data, function (error, result) {
            if (error) {
                client.sendErrorResponse(response, error, httpStatus.INTERNAL_SERVER_ERROR);
            }
            else if (result.success) {
                client.sendObjectResponse(response, result);
            }
            else{
                client.sendObjectResponse(response, result);
            }
        }
    );
});

//remove  autoAlert associate to  specific topic
router.post('/removeAlert', function (request, response) {
    notification.removeAlert(request.body.data, function (error, result) {
            if (error) {
                client.sendErrorResponse(response, error, httpStatus.INTERNAL_SERVER_ERROR);
            }
            else if (result.success) {
                client.sendObjectResponse(response, result);
            }
            else{
                client.sendObjectResponse(response, result);
            }
        }
    );
});
router.post('/sendToClass', function (request, response) {
    var i;
    var res = null;
    for (i=0 ; i< request.body.data.usersInClass.length ; i++){
        var data = {
            title:request.body.data.title,
            message:request.body.data.message,
            to:request.body.data.usersInClass[i],
            url:request.body.data.url,
            flag:request.body.data.flag,
            image:request.body.data.image
        };
        notification.message_to_client(data, function (error, result) {
                if (error) {
                    client.sendErrorResponse(response, error, httpStatus.INTERNAL_SERVER_ERROR);
                }
                res = result;
            }
        );
    }
    client.sendObjectResponse(response, {obj: res, success: true});

});

module.exports = router;
