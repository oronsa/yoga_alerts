var router = require('express').Router();
var client = require('../../utils/clientUtils/clientUtil');
var config = require('../../../config.json');
var ROLES = config.ROLES;
var PATHS = config.PATHS;

//check if user is logged in to the system
router.get('/isLoggedIn', function(request, response) {
    if(request.session && request.session.userId) {
        client.sendObjectResponse(response, { userId : request.session.userId,type : request.session.type});
    }
    else {
        client.sendObjectResponse(response, { userId : null, type : ROLES.GUEST });
    }
});

// check of user is authorized to inter to specific page
router.post('/isAuthorized', function(request, response) {
    var userData = JSON.parse(request.body.userData);
    var type = userData.type;
    var userId = userData.userId;
    var action = userData.action;

    if(PATHS[type].hasOwnProperty(action)) {
        if(type === ROLES.ADMIN && action === PATHS.guest.logIn){
            client.sendObjectResponse(response, { isAuthorized : false, userId : userId , action : 'adminHome'});
        }
        else {
            client.sendObjectResponse(response, { isAuthorized : true, userId : userId , action : action, type : type });
        }
    }
    //is guest
    else {
        client.sendObjectResponse(response, { isAuthorized : false, userId : '', action: PATHS.guest.logIn });
    }
});

module.exports = router;