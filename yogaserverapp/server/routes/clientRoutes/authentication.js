var express = require('express');
var router = express.Router();
var authenticator = require('../../utils/clientUtils/authUtils');
var httpStatus = require('http-status-codes');
var email = require("../../utils/adminUtils/email");
var client = require('../../utils/clientUtils/clientUtil');

router.post('/signUp', function (request, response) {

    authenticator.signUp(request.body, function (error, result) {
            if (error) {
                client.sendErrorResponse(response, error, httpStatus.INTERNAL_SERVER_ERROR);
            }
            else if (result.success) {
                client.sendObjectResponse(response, result);
                email.welcomeEmail(request.body.email,request.body.email);
            }
            else{
                client.sendObjectResponse(response, result);
            }
        }
    );
});
router.post('/updateToken', function (request, response) {

    authenticator.updateToken(request.body, function (error, result) {
            if (error) {
                client.sendErrorResponse(response, error, httpStatus.INTERNAL_SERVER_ERROR);
            }
            else if (result.success) {
                client.sendObjectResponse(response, result);
            }
            else{
                client.sendObjectResponse(response, result);
            }
        }
    );
});
router.post('/updateTokenSignIn', function (request, response) {

    authenticator.updateTokenSignIn(request.body, function (error, result) {
            if (error) {
                client.sendErrorResponse(response, error, httpStatus.INTERNAL_SERVER_ERROR);
            }
            else if (result.success) {
                client.sendObjectResponse(response, result);
            }
            else{
                client.sendObjectResponse(response, result);
            }
        }
    );
});
router.post('/checkToken', function (request, response) {

    authenticator.checkToken(request.body, function (error, result) {
            if (error) {
                client.sendErrorResponse(response, error, httpStatus.INTERNAL_SERVER_ERROR);
            }
            else if (result.success) {
                // email.welcomeEmail(userData.email, userData.firstName);
                client.sendObjectResponse(response, result);
            }
            else{
                client.sendObjectResponse(response, result);
            }
        }
    );
});
router.post('/getDetails', function (request, response) {

    authenticator.getDetails(request.body, function (error, result) {
            if (error) {
                client.sendErrorResponse(response, error, httpStatus.INTERNAL_SERVER_ERROR);
            }
            else if (result.success) {
                // email.welcomeEmail(userData.email, userData.firstName);
                client.sendObjectResponse(response, result);
            }
            else{
                client.sendObjectResponse(response, result);
            }
        }
    );
});
router.post('/importTopics',function(request,response){

    authenticator.importTopics(request,function (error, result) {
            if (error) {
                client.sendErrorResponse(response, error, httpStatus.INTERNAL_SERVER_ERROR);
            }
            else if (result.success) {
                // email.welcomeEmail(userData.email, userData.firstName);
                client.sendObjectResponse(response, result);
            }
            else{
                client.sendObjectResponse(response, result);
            }
        }
    );
});
router.post('/updateDetails', function (request, response) {

    authenticator.updateDetails(request.body, function (error, result) {
            if (error) {
                client.sendErrorResponse(response, error, httpStatus.INTERNAL_SERVER_ERROR);
            }
            else if (result.success) {
                client.sendObjectResponse(response, result);
            }
            else{
                client.sendObjectResponse(response, result);
            }
        }
    );
});
router.post('/editPassword', function (request, response) {

    authenticator.editPassword(request.body, function (error, result) {
            if (error) {
                client.sendErrorResponse(response, error, httpStatus.INTERNAL_SERVER_ERROR);
            }
            else if (result.success) {
                client.sendObjectResponse(response, result);
            }
            else{
                client.sendObjectResponse(response, result);
            }
        }
    );
});
router.post('/updatePassword', function (request, response) {

    authenticator.updatePassword(request.body, function (error, result) {
            if (error) {
                client.sendErrorResponse(response, error, httpStatus.INTERNAL_SERVER_ERROR);
            }
            else if (result.success) {
                client.sendObjectResponse(response, result);
            }
            else{
                client.sendObjectResponse(response, result);
            }
        }
    );
});
router.post('/checkEmail', function (request, response) {

    authenticator.checkEmail(request.body, function (error, result) {
            if (error) {
                client.sendErrorResponse(response, error, httpStatus.INTERNAL_SERVER_ERROR);
            }
            else if (result.success) {
                client.sendObjectResponse(response, result);
            }
            else{
                client.sendObjectResponse(response, result);
            }
        }
    );
});
module.exports = router;
