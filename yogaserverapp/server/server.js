var express = require('express');
var app =express();
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var admin = require('firebase-admin');
var session = require('client-sessions');

app.use(cookieParser());
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());
app.use(session({
    cookieName : 'session',
    secret : 'yogaSecret',
    duration: 30 * 60 * 1000,
    activeDuration: 5 * 60 * 1000,
    httpOnly: true,
    secure: true,
    ephemeral: true
}));

app.use(express.static(__dirname + '/../client'));
app.use(express.static(__dirname + '/../'));

// Fetch the service account key JSON file contents
var serviceAccount = require("../yoga-alerts-firebase-adminsdk-hghy9-66da93ab8a.json");
var request = require("request");
var moment = require("moment");

// Initialize the app with a service account, granting admin privileges
admin.initializeApp({
 credential: admin.credential.cert(serviceAccount),
 databaseURL: "https://yoga-alerts.firebaseio.com"
});

// app.use(express.static(__dirname + '/../client'));
// app.use(express.static(__dirname + '/../'));


app.use('/api/authentication', require('./routes/clientRoutes/authentication'));
app.use('/admin/api/authentication', require('./routes/adminRoutes/authentication'));
app.use('/api/session', require('./routes/adminRoutes/session'));
app.use('/api/database', require('./routes/database'));
app.use('/api/notification', require('./routes/adminRoutes/notificationRoutes'));

//
app.all('*', function(request, response) {
    response.sendFile('index.html', {
        root : __dirname + '/../client'
    });
});

moment().format('DD-MM-YYYY');

app.use(function(request, response) {
    response.redirect('/');
});


module.exports = app;
