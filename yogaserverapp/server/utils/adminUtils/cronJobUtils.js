var express = require('express');
var httpStatus = require('http-status-codes');
var CronJob = require('cron').CronJob;
var config = require('../../../config.json');
var COLLECTIONS = config.COLLECTIONS;
notification = require('../../utils/notifications');
var client = require('../../utils/clientUtils/clientUtil');
var mongoUtils = require('../../utils/mongoUtils');
notification = require('../../utils/notifications');
var moment = require('moment');

new CronJob('30 * * * * *', function () {
    var date = new Date();
    console.log(date.getHours() + " " + date.getMinutes() + " " + date.getDay());
    mongoUtils.query(COLLECTIONS.ALERTS, {}, function (error, result) {
        if (error) {
            console.log(error);
        } else if (result) {
            var i, fromDay, toDay;
            for (i = 0; i < result.length; i++) {
                switch (result[i].fromDay) {
                    case 'ראשון':
                        fromDay = 0;
                        break;
                    case 'שני':
                        fromDay = 1;
                        break;
                    case 'שלישי':
                        fromDay = 2;
                        break;
                    case 'רביעי':
                        fromDay = 3;
                        break;
                    case 'חמישי':
                        fromDay = 4;
                        break;
                    case 'שישי':
                        fromDay = 5;
                        break;
                    case 'שבת':
                        fromDay = 6;
                        break;
                    default:
                        break;
                }
                switch (result[i].toDay) {
                    case 'ראשון':
                        toDay = 0;
                        break;
                    case 'שני':
                        toDay = 1;
                        break;
                    case 'שלישי':
                        toDay = 2;
                        break;
                    case 'רביעי':
                        toDay = 3;
                        break;
                    case 'חמישי':
                        toDay = 4;
                        break;
                    case 'שישי':
                        toDay = 5;
                        break;
                    case 'שבת':
                        toDay = 6;
                        break;
                    default:
                        break;
                }
                //check if we are in range, we will send the notification
                if (result[i].min == date.getMinutes() && result[i].hours == date.getHours() &&
                    fromDay <= date.getDay() && toDay >= date.getDay()) {
                    result[i].flag = '';
                    result[i].image = '';
                    if (result[i].topic==="is_class_alert") {
                        for (var j = 0; j < result[i].token_list.length; j++) {
                            var dataToSend = {
                                title:result[i].title,
                                message:result[i].message,
                                url:result[i].url,
                                image:'',
                                flag:'',
                                to:result[i].token_list[j]
                            };
                            notification.message_to_client(dataToSend, function (error, result) {
                                if (error) {
                                    client.sendErrorResponse(null, error, httpStatus.INTERNAL_SERVER_ERROR);
                                }
                                else if (result.success) {
                                    client.sendObjectResponse(null, result);
                                }
                                else {
                                    client.sendObjectResponse(null, result);
                                }
                            });
                        }
                    }
                    else{
                        notification.message_to_topic(result[i], function (error, result) {
                                if (error) {
                                    client.sendErrorResponse(null, error, httpStatus.INTERNAL_SERVER_ERROR);
                                }
                                else if(result) {
                                    if (result.success) {
                                        client.sendObjectResponse(null, result);
                                    }
                                    else {
                                        client.sendObjectResponse(null, result);
                                    }
                                }
                            }
                        );
                    }
                }
            }
            client.sendObjectResponse(null, result);
        }
    });
}, null, true, 'Asia/Jerusalem');

//cron job for birthday message
new CronJob('00 00 11 * * 0-6', function () {
    mongoUtils.query(COLLECTIONS.USERS, {}, function (error, result) {
        if (error)
            console.log(error);
        else if (result) {
            var currDate = new Date();
            console.log(currDate);
            for (var i = 0; i < result.length; i++) {
                var parts = result[i].birthday.split('/');
                if (currDate.getDate() == parts[0] && currDate.getMonth()+1 == parts[1]) {

                    var data = {
                        title:"מזל טוב ליום הולדתך",
                        message:"\"מי שחי יותר אינו מי שחי הרבה שנים, אך מי שחש יותר בחיים\" ז\'אן-ז\'אק רוסו." +
                        "מאחלת לך עוד הרבה שנים טובות, מלאות שמחה, חברות, אחווה ותרגול יוגה.",
                        flag:"birthday",
                        image:'http://www.planwallpaper.com/static/images/Happy-Birthday-HD-Images-3.jpg',
                        url:'',
                        to: result[i].firebaseToken
                    };
                    notification.message_to_client(data, function (error, result) {
                        if (error) {
                            client.sendErrorResponse(null, error, httpStatus.INTERNAL_SERVER_ERROR);
                        }
                        else if (result.success) {
                            //TODO: welcome email
                            client.sendObjectResponse(null, result);
                        }
                        else {
                            client.sendObjectResponse(null, result);
                        }
                    });
                }
            }
        }
    });
}, null, true, 'Asia/Jerusalem');

//cron job for membership alert 14 days before
new CronJob('00 31 13 * * 0-6', function () {

    mongoUtils.query(COLLECTIONS.USERS, {}, function (error, result) {
        if (error)
            console.log(error);
        else if (result) {
            var currDate = new Date();
            for (var i = 0; i < result.length; i++) {
                if (result[i].membershipDate) {
                    var parts = result[i].membershipDate.split('/');
                    var tempDate = new Date(parts[2], parseInt(parts[1]) - 1, parts[0]);
                    tempDate.setDate(tempDate.getDate() + 14);
                    currDate.setHours(0, 0, 0, 0);
                    tempDate.setHours(0, 0, 0, 0);
                    if (moment(currDate).isSame(tempDate, 'day')) {

                        var data = {
                            title: "תזכורת לגבי עדכון מנוי",
                            message: "אני מקווה שאתם נהנים משיעורי היוגה ורואים את ההתקדמות. שימו לב שבעוד 14 ימים יסתיים המנוי\כרטיסייה שרכשתם. אני מקווה שאתם רוצים להמשיך לתרגל. אנא צרו קשר כדי לחדש את המנוי\כרטיסייה.",
                            flag: "contact",
                            image: '',
                            url: '',
                            to: result[i].firebaseToken
                        };
                        notification.message_to_client(data, function (error, result) {
                            if (error) {
                                client.sendErrorResponse(null, error, httpStatus.INTERNAL_SERVER_ERROR);
                            }
                            else if (result.success) {
                                //TODO: welcome email
                                client.sendObjectResponse(null, result);
                            }
                            else {
                                client.sendObjectResponse(null, result);
                            }
                        });
                    }
                }
            }
        }
    });
}, null, true, 'Asia/Jerusalem');

//cron job for membership alert in the specific date
new CronJob('00 30 13 * * 0-6', function () {

    mongoUtils.query(COLLECTIONS.USERS, {}, function (error, result) {
        if (error)
            console.log(error);
        else if (result) {
            var currDate = new Date();
            for (var i = 0; i < result.length; i++) {
                if(result[i].membershipDate) {
                    var parts = result[i].membershipDate.split('/');
                    var tempDate = new Date(parts[2], parseInt(parts[1]) - 1, parts[0]);
                    if (moment(currDate).isSame(tempDate, 'day')) {
                        var data = {
                            title: "הודעה לגבי יום סיום המנוי",
                            message: "היום תם תוקף המנוי\כרטיסייה שרכשתם. אני מקווה שנתראה עוד השבוע לחידוש המנוי\כרטיסייה. מאחלת המשך דרך מבורכת ביוגה.",
                            flag: "contact",
                            image: '',
                            url: '',
                            to: result[i].firebaseToken
                        };
                        notification.message_to_client(data, function (error, result) {
                            if (error) {
                                client.sendErrorResponse(null, error, httpStatus.INTERNAL_SERVER_ERROR);
                            }
                            else if (result.success) {
                                //TODO: welcome email
                                client.sendObjectResponse(null, result);
                            }
                            else {
                                client.sendObjectResponse(null, result);
                            }
                        });
                    }
                }
            }
        }
    });
}, null, true, 'Asia/Jerusalem');

