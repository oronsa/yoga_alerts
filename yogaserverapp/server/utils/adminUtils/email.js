var helper = require('sendgrid').mail;
var sg = require('sendgrid')('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');


var from_email = new helper.Email('agi@agiyoga.co.il');

function sendMail(to, subject, content) {
    var to_email = new helper.Email(to);
    var subject = subject;
    var content = new helper.Content('text/plain',content);
    var mail = new helper.Mail(from_email, subject, to_email,content);
    var request = sg.emptyRequest({
        method: 'POST',
        path: '/v3/mail/send',
        body: mail.toJSON()
    });
    sg.API(request, function (error, info) {
        if (error) {
            console.log(error);
            return;
        }
        console.log('Message sent: ' + info.response);
    });
}

module.exports = {

    tokenEmail:  function (to, name, token) {
        var body =token+" מפתח ההרשמה שלך לאפליקצייה הוא "+name+" שלום ";
        sendMail(to, "מפתח הרשמה עבור איינגר יוגה עם אגי ", body);
    },

    welcomeEmail : function (to, username) {
        var body ="ברכות על הצטרפותך לאפליקציה החדשה שלנו שתקל עליך בתרגול היוגה בסטודיו ובבית";
        sendMail(to, "ברוכים הבאים", body);
    },
    recoveryMail :function (to, username, tempPassword){
        console.log(to+" "+username+" "+tempPassword);
        var body = tempPassword+" הסיסמא הזמנית שלך היא: "+username+" שלום ";
        sendMail(to, "שחזור סיסמא אגי יוגה", body);
    }
};
