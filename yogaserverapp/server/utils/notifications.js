/**
 * Created by oronsa on 11/12/2016.
 */
var admin = require('firebase-admin');
var request = require('request');
var mongoUtils = require('../utils/mongoUtils');
var config = require('../../config.json');
var COLLECTIONS = config.COLLECTIONS;

var API_KEY = "AAAAaQuJdYI:APA91bGt523tC2TyJDpuPiIjfdQ55z8bwQXu5wdV6QZ8XjhBM" +
    "uQsPnuIPvaQ8OfWQoPFT_yIJcJHdwLEm9fsWnTb992DqwKIC7hjFjRCCiJRnoagWSbKXnHL" +
    "R12VU8jRmgykpN4NqIGNJaely0X8AIxhoTB1i25kFg"; // My Firebase Cloud Messaging Server API key

ref = admin.database().ref("notifications");
module.exports = {

    createNewAlert: function (data, callBack) {
        var tokenList = [];
        //if this auto alerts for topic
        if(data.selectedClass ==''){
            mongoUtils.insert(COLLECTIONS.ALERTS, {
                    'time': data.time, 'fromDay': data.selectedFromDay,'min':data.min,'hours':data.hours,'url':data.url
                    , 'toDay': data.selectedToDay, 'topic': data.selectedTopic, 'title': data.title, 'message': data.message
                },
                function (error, result) {
                    if (error) {
                        callBack(error, null);
                    }
                    else {
                        callBack(null, {obj: result.ops[0], success: true});
                    }
                });
        }else {
            mongoUtils.query(COLLECTIONS.USERS, {class: {name: data.selectedClass}},function (error,result) {
                if(result) {
                    if (result.length > 0) {
                        for (var i = 0; i < result.length; i++) {
                            tokenList[i] = result[i].firebaseToken;
                        }
                        mongoUtils.insert(COLLECTIONS.ALERTS, {
                                'time': data.time,
                                'fromDay': data.selectedFromDay,
                                'min': data.min,
                                'hours': data.hours,
                                'url': data.url
                                ,
                                'toDay': data.selectedToDay,
                                'topic': 'is_class_alert',
                                'title': data.title,
                                'message': data.message,
                                token_list: tokenList
                            },
                            function (error, result) {
                                if (error) {
                                    callBack(error, null);
                                }
                                else {
                                    callBack(null, {obj: result.ops[0], success: true});
                                }
                            });
                    }else{
                        callBack(null, {msg: 'noSignToClass'});
                    }
                }else{
                    callBack(error, null);
                }
            });
        }
    },
    removeAlert: function (data, callBack) {
        mongoUtils.delete(COLLECTIONS.ALERTS, {'message':data},
            function (error) {
                if (error) {
                    callBack(error, null);
                }
                else {
                    callBack(null, {success: true});
                }
            });
    },
    listenForNotificationRequests: function ()
    {
        var requests = ref.child('notificationRequests');
        ref.on('child_added', function (requestSnapshot) {
            var request = requestSnapshot.val();
            sendNotificationToUser(
                request.username,
                request.message,
                function () {
                    requestSnapshot.ref.remove();
                }
            );
        }, function (error) {
            console.error(error);
        });
    },
    message_to_client: function (data,callback) {
        request({
            url: 'https://fcm.googleapis.com/fcm/send',
            method: 'POST',
            headers: {
                'Content-Type': ' application/json',
                'Authorization': 'key=' + API_KEY
            },
            body: JSON.stringify({
                data: {
                    title: data.title,
                    body: data.message,
                    url:data.url,
                    image:data.image,
                    flag:data.flag

                },
                to:data.to
            })
        }, function (error, result) {
            if (error) {
                console.error(error);
                callback(error, null);
            }
            else if (result.statusCode >= 400) {
                console.error('HTTP Error: ' + result.statusCode + ' - ' + result.statusMessage);
            }
            else {
                callback(null, {obj: result, success: true});
            }
        });
    },
    sendToBlog: function (data,callback) {
        request({
            url: 'https://fcm.googleapis.com/fcm/send',
            method: 'POST',
            headers: {
                'Content-Type': ' application/json',
                'Authorization': 'key=' + API_KEY
            },
            body: JSON.stringify({
                data: {
                    title: data.title,
                    body: data.body,
                    flag:'blogFlag'
                },
                to:"/topics/default"
            })
        }, function (error, result) {
            if (error) {
                console.error(error);
                callback(error, null);
            }
            else if (result.statusCode >= 400) {
                console.error('HTTP Error: ' + result.statusCode + ' - ' + result.statusMessage);
            }
            else {
                callback(null, {obj: result, success: true});
            }
        });
    },
    message_to_topic: function (data,callback) {
        request({
            url: 'https://fcm.googleapis.com/fcm/send',
            method: 'POST',
            headers: {
                'Content-Type': ' application/json',
                'Authorization': 'key=' + API_KEY
            },
            body: JSON.stringify({
                data: {
                    title: data.title,
                    body: data.message,
                    url:data.url,
                    image:data.image,
                    flag:data.flag
                },
                to: "/topics/" + data.topic
            })
        }, function (error, result) {
            if (error) {
                console.error(error);
                callback(error, null);
            }
            else if (result.statusCode >= 400) {
                console.error('HTTP Error: ' + result.statusCode + ' - ' + result.statusMessage);
            }
            else {
                callback(null, {obj: result, success: true});
            }
        });
    },
    paymentToClient: function (data,callback) {
        request({
            url: 'https://fcm.googleapis.com/fcm/send',
            method: 'POST',
            headers: {
                'Content-Type': ' application/json',
                'Authorization': 'key=' + API_KEY
            },
            body: JSON.stringify({
                data: {
                    title: data.title,
                    body: data.message,
                    image:'',
                    flag:'payment'
                },
                to:data.to
            })
        }, function (error, result) {
            if (error) {
                console.error(error);
                callback(error, null);
            }
            else if (result.statusCode >= 400) {
                console.error('HTTP Error: ' + result.statusCode + ' - ' + result.statusMessage);
            }
            else {
                callback(null, {obj: result, success: true});
            }
        });
    }
};
