var mongoUtils = require('../mongoUtils');
var bcryptJS = require('bcryptjs');
var config = require('../../../config.json');
var randToken = require('rand-token');

var COLLECTIONS = config.COLLECTIONS;

module.exports = {
    //client signing up to the service
    signUp: function (userData, callback) {
        mongoUtils.query(COLLECTIONS.USERS, {'email': {$eq: userData.email}},
            function (error, result) {
                if (error) {
                    callback(error, null);
                }
                else if(result) {
                    if (result.length) {
                        callback(null, {msg: 'alreadySignedUp', success: "false"});
                    }
                    else {
                        var salt = bcryptJS.genSaltSync(8);
                        userData.password = bcryptJS.hashSync(userData.password, salt);
                        mongoUtils.delete(COLLECTIONS.TOKEN_ACCOUNTS, {token: userData.token},
                            function (error, result) {
                                if (error) {
                                    callback(error, null);
                                }
                                else {
                                    mongoUtils.insert(COLLECTIONS.USERS, userData,
                                        function (error, result) {
                                            if (error) {
                                                callback(error, null);
                                            }
                                            else {
                                                callback(null, {obj: result, success: "true"});
                                            }
                                        }
                                    );
                                }
                            }
                        );
                    }
                }
            }
        );
    },
    //check if the token is correct for current email address before signing up
    checkToken:function(userData,callback){
        mongoUtils.query(COLLECTIONS.TOKEN_ACCOUNTS, {'email': {$eq: userData.email}, 'token': {$eq: userData.token}},
            function (error, result) {
                if (error) {
                    callback(error, null);
                }
                else if(result) {
                    if (!result.length) {
                        callback(null, {msg: 'missingToken', success: "wrongToken"});
                    }
                    else {
                        callback(null, {obj: result, success: "true"});
                    }
                }
            });
    },
    //update token is DB when firebase change the token
    updateToken: function (userData, callback) {
        mongoUtils.query(COLLECTIONS.USERS, {'firebaseToken': {$eq: userData.oldToken}},
            function (error, result) {
                if (error) {
                    callback(error, null);
                }
                else if(result) {
                    if (!result.length) {
                        callback(null, {msg: 'error', success: false});
                    }
                    else if (result.length == 1) {
                        {
                            mongoUtils.update(COLLECTIONS.USERS, {firebaseToken: userData.oldToken}, {$set: {firebaseToken: userData.newToken}}, {},
                                function (error, result) {
                                    if (error) {
                                        callback(error, null);
                                    }
                                    else {
                                        callback(null, {obj: result, success: true});
                                    }
                                });
                        }
                    }
                }
            });
    },
    //update the token when the user sign in to application(because the token sometimes change and we need manually update
    updateTokenSignIn: function (userData, callback) {
        mongoUtils.query(COLLECTIONS.USERS, {'email': {$eq: userData.email}},
            function (error, result) {
                if (error) {
                    callback(error, null);
                }
                if(result){
                    if (!result.length) {
                        callback(null, {msg: 'error', success: false});
                    }
                    else if (result.length == 1) {
                        {
                            mongoUtils.update(COLLECTIONS.USERS, {email: userData.email}, {$set: {firebaseToken: userData.token}}, {},
                                function (error, result) {
                                    if (error) {
                                        callback(error, null);
                                    }
                                    else {
                                        callback(null, {obj: result, success: true});
                                    }
                                });
                        }
                    }
                }
            });
    },
    //get user details from DB
    getDetails:function (userData,callback) {
        mongoUtils.query(COLLECTIONS.USERS, {'email': {$eq: userData.email}},
            function (error, result) {
                if (error) {
                    callback(error, null);
                }
                else if (result) {
                    if (!result.length) {
                        callback(null, {msg: 'noSuchUser', success: "false"});
                    }
                    else {
                        callback(null, {obj: result, success: "true"});
                    }
                }
            });
    },
    //get topics to subscribe from DB
    importTopics:function (data,callback) {
        mongoUtils.query(COLLECTIONS.TOPICS,{},function (error,result) {
            if (error) {
                callback(error, null);
            }
            else if(result) {
                if (!result.length) {
                    callback(null, {msg: 'some error', success: "false"});
                }
                else {
                    callback(null, {obj: result, success: "true"});
                }
            }
        });

    },
    //call when user update manually his details
    updateDetails:function (userData,callback) {
        mongoUtils.update(COLLECTIONS.USERS, {email: userData.oldMail}, {$set: {
                name: userData.name,lastName:userData.lastName,email:userData.email,birthday:userData.birthday,phone:userData.phone}}, {},
            function (error, result) {
                if (error) {
                    callback(error, null);
                }
                else if (result) {

                    if (!result.length) {
                        callback(null, {msg: 'update fail', success: false});
                    }
                    else {
                        callback(null, {obj: result, success: true});
                    }
                }
            });
    },
//when user forgot his password
    updatePassword:function (userData,callback) {
        var salt = bcryptJS.genSaltSync(8);
        userData.password = bcryptJS.hashSync(userData.password, salt);
        mongoUtils.update(COLLECTIONS.USERS, {email: userData.email}, {$set: {password: userData.password}}, {},
            function (error, result) {
                if (error) {
                    callback(error, null);
                }
                else if (result) {
                    if (!result.length) {
                        callback(null, {msg: 'update fail', success: false});
                    }
                    else {
                        callback(null, {obj: result, success: true});
                    }
                }
            });
    },
//when user wants to update his password
    editPassword: function (userData, callBack) {
        mongoUtils.query(COLLECTIONS.USERS, {'email': userData.email}, function (error, result) {
            if (error) {
                callBack(error, null);
            }
            else if(result) {
                if (!result.length) {
                    callBack(null, {success: false});
                }
                else if (result.length == 1) {
                    var userObj = result[0];
                    if (bcryptJS.compareSync(userData.password, userObj.password)) {
                        var salt = bcryptJS.genSaltSync(8);
                        userData.newPassword = bcryptJS.hashSync(userData.newPassword, salt);
                        mongoUtils.update(COLLECTIONS.USERS, {email: userData.email}, {$set: {password: userData.newPassword}}, {},
                            function (error, result) {
                                if (error) {
                                    callBack(error, null);
                                }
                                else {
                                    callBack(null, {obj: result, success: "true"});
                                }
                            }
                        );

                    }
                    else {
                        callBack(null, {msg: 'invalidPassword', success: "false"});
                    }
                }
            }
        });
    },
//for the first step of recover password we need to check if this user exist
    checkEmail: function (userData, callBack) {
        mongoUtils.query(COLLECTIONS.USERS, {'email': userData.email}, function (error, result) {
            if (error) {
                callBack(error, null);
            }
            else if(result) {
                if (!result.length) {
                    callBack(null, {success: "false"});
                }
                else if (result.length == 1) {
                    callBack(null, {success: "true"});
                }
            }
        });
    }
};