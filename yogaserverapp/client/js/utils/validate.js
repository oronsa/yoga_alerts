angular.module('yogaAdmin')

    //
    .factory('validateUtils', function ($rootScope) {

        var validateEmail = function validateEmail(email) {
            if (!email) {
                swal({   title: "שגיאה!",
                    text: " שים לב: מייל אינו תקין",
                    type: "warning"
                });
                return false;
            }
            return true;
        };
        var validateToken = function validateToken(token) {
            if (!token) {
                swal({   title: "שגיאה!",
                    text: "מפתח אינו תקין בדוק את המפתח אשר נשלח אליך למייל",
                    type: "warning"
                });
                return false;
            }
            return true;
        };

        var validatePassword = function validatePassword(password) {
            if (!password) {
                swal({   title: "שגיאה!",
                    text: "סיסמא צריכה להכיל אות קטנה אות גדולה ומספרים באורך של 6 תווים לפחות",
                    type: "error"
                });
                return false;
            }
            return true;
        };

        var validateConfirmPassword = function validateConfirmPassword(confirmPassword) {
            if (!confirmPassword) {
                swal({   title: "שגיאה!",
                    text: "אישור סיסמא  סיסמא צריך להכיל אות קטנה אות גדולה ומספרים באורך של 6 תווים לפחות",
                    type: "warning"
                });
                return false;
            }
            return true;
        };
         function validateUrl(link) {
            if (!link) {
                swal({   title: "שגיאה!",
                    text: "יש להזין כתובת חוקית, או לבטל את סימון הוספת הלינק נסה שוב",
                    type: "warning"
                });
                return false;
            }
            return true;
        }
         function validateTopic(topic) {
            if (!topic) {
                swal({   title: "שגיאה!",
                    text: "  הנושא צריך להכיל אותיות באנגלית בלבד עד 50 תווים וללא רווחים למשל :example_topic",
                    type: "warning"
                });
                return false;
            }
            return true;
        }
        function validateTopicHebrew(topic) {
            if (!topic) {
                swal({   title: "שגיאה!",
                    text: "  הנושא צריך להכיל אותיות בעברית בלבד ובאורך של עד 50 תווים",
                    type: "warning"
                });
                return false;
            }
            return true;
        }
        function validateAmount(amount){
            if (!amount) {
                swal({   title: "שגיאה!",
                    text: "הסכום אינו תקין,הזן לדוגמא :120.00",
                    type: "warning"
                });
                return false;
            }
            return true;
        }

        function validateForm(userData) {
            var returnValue;
            returnValue = validateEmail(userData.email);
            if (!returnValue) {
                return false;
            }
            returnValue = validateToken(userData.token);
            if (!returnValue) {
                return false;
            }
            returnValue = validatePassword(userData.password);
            if (!returnValue) {
                return false;
            }
            returnValue = validateConfirmPassword(userData.confirmPassword);
            if (!returnValue) {
                return false;
            }
            if(userData.password!=userData.confirmPassword){
                swal({   title: "שגיאה!",
                    text: " שים לב: סיסמא ואישור סיסמא צריכים להיות זהות",
                    type: "warning"
                });
                return false;
            }
            return true;
        }

        return {
            validateUrl:validateUrl,
            validateTopic:validateTopic,
            validateForm: validateForm,
            validateEmail:validateEmail,
            validateToken:validateToken,
            validatePassword:validatePassword,
            validateConfirmPassword:validateConfirmPassword,
            validateTopicHebrew:validateTopicHebrew,
            validateAmount:validateAmount
        };
    });