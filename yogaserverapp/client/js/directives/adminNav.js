
angular.module('yogaAdmin').directive('adminNav', function () {
    return {
        restrict : 'E',
        replace : true,
        templateUrl : 'js/templates/adminNav.html',
        controller : function($scope, $rootScope,yogaService, $state,COLLECTIONS) {
            $scope.logOut = function() {
                $rootScope.logOut();
            };
            $scope.switchState = function(state) {
                $state.go(state);
            };
            yogaService.query(COLLECTIONS.ADMINS,{'_id' :$rootScope.userSession.userId }).then(function(result) {
                $scope.email= result.collection[0].email;
            })
        }
    };
});