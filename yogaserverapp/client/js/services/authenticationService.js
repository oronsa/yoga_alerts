angular.module('yogaAdmin')

//
    .factory('authenticationService', function ($resource, $q) {

        var baseUrl = '/admin/api/authentication';
        function changePassword(userDetails){
            var deferred = $q.defer();
            $resource(baseUrl + '/changePassword').save({ userData:JSON.stringify(userDetails)}, function (result, error) {
                deferPromise(deferred, result, error);
            });
            return deferred.promise;
        }
        function editPassword(userDetails){
            var deferred = $q.defer();
            $resource(baseUrl + '/editPassword').save({ userData:JSON.stringify(userDetails)}, function (result, error) {
                deferPromise(deferred, result, error);
            });
            return deferred.promise;
        }
        function recovery(userDetails){
            var deferred = $q.defer();
            $resource(baseUrl + '/recovery').save({ userData: JSON.stringify(userDetails) }, function (result, error) {
                deferPromise(deferred, result, error);
            });
            return deferred.promise;
        }
        function logIn(userData) {
            var deferred = $q.defer();
            $resource(baseUrl + '/logIn').save({ userData : JSON.stringify(userData) }, function (result, error) {
                deferPromise(deferred, result, error);
            });
            return deferred.promise;
        }

        function logOut() {
            var deferred = $q.defer();
            $resource(baseUrl + '/logOut').get(function (result, error) {
                deferPromise(deferred, result, error);
            });
            return deferred.promise;
        }
        function changeEmail(currentEmail, newEmail, userId) {
            var deferred = $q.defer();
            $resource(baseUrl + '/changeEmail').save({ currentEmail : currentEmail, newEmail : newEmail, userId : userId}, function (result, error) {
                deferPromise(deferred, result, error);
            });
            return deferred.promise;
        }

        function applyNewEmail(newEmail, token) {
            var deferred = $q.defer();
            $resource(baseUrl + '/applyNewEmail').save({ newEmail : newEmail, token : token }, function (result, error) {
                deferPromise(deferred, result, error);
            });
            return deferred.promise;
        }
        function signUp(userDetails) {
            var deferred = $q.defer();
            $resource(baseUrl + '/signUp').save({ userData: JSON.stringify(userDetails) }, function (result, error) {
                deferPromise(deferred, result, error);
            });
            return deferred.promise;
        }
        function createNewTopic(data){
            var deferred = $q.defer();
            $resource(baseUrl + '/createNewTopic').save({ data:JSON.stringify(data)}, function (result, error) {
                deferPromise(deferred, result, error);
            });
            return deferred.promise;
        }

        function deleteUser(data) {
            var deferred = $q.defer();
            $resource(baseUrl + '/deleteUser').remove({
                    userData:JSON.stringify(data)},
                function (result, error) {
                    deferPromise(deferred, result, error);
                });
            return deferred.promise;
        }
        function sendToken(data) {
            var deferred = $q.defer();
            $resource(baseUrl + '/sendToken').save({ userData: JSON.stringify(data)}, function (result, error) {
                deferPromise(deferred, result, error);
            });
            return deferred.promise;
        }
        return {
            applyNewEmail : applyNewEmail,
            changeEmail : changeEmail,
            logIn : logIn,
            logOut : logOut,
            signUp: signUp,
            recovery: recovery,
            changePassword : changePassword,
            deleteUser:deleteUser,
            sendToken:sendToken,
            createNewTopic:createNewTopic,
            editPassword:editPassword
        };

        function deferPromise(deferred, result, error) {
            if (result) {
                deferred.resolve(result);
            }
            else {
                deferred.reject(error);
            }
        }
    });