
angular.module('yogaAdmin')

//
    .factory('notificationService', function ($resource, $q) {

        var baseUrl = '/api/notification';
        function sendToTopic(data) {
            var deferred = $q.defer();
            $resource(baseUrl + '/sendToTopic').save({data:data}, function (result, error) {
                deferPromise(deferred, result, error);
            });
            return deferred.promise;
        }
        function sendToBlog(data){
            var deferred = $q.defer();
            $resource(baseUrl + '/sendToBlog').save({data:data}, function (result, error) {
                deferPromise(deferred, result, error);
            });
            return deferred.promise;
        }
        function sendToClient(data) {
            var deferred = $q.defer();
            $resource(baseUrl + '/sendToClient').save({data:data}, function (result, error) {
                deferPromise(deferred, result, error);
            });
            return deferred.promise;
        }
        function createNewAlert(data) {
            var deferred = $q.defer();
            $resource(baseUrl + '/createNewAlert').save({data:data}, function (result, error) {
                deferPromise(deferred, result, error);
            });
            return deferred.promise;
        }
        function removeAlert(data) {
            var deferred = $q.defer();
            $resource(baseUrl + '/removeAlert').save({data:data}, function (result, error) {
                deferPromise(deferred, result, error);
            });
            return deferred.promise;
        }
        function paymentToClient(data) {
            var deferred = $q.defer();
            $resource(baseUrl + '/paymentToClient').save({data:data}, function (result, error) {
                deferPromise(deferred, result, error);
            });
            return deferred.promise;
        }
        function sendToClass(data){
            var deferred = $q.defer();
            $resource(baseUrl + '/sendToClass').save({data:data}, function (result, error) {
                deferPromise(deferred, result, error);
            });
            return deferred.promise;
        }
        return {
            sendToTopic : sendToTopic,
            sendToClient : sendToClient,
            createNewAlert : createNewAlert,
            removeAlert:removeAlert,
            sendToBlog:sendToBlog,
            paymentToClient:paymentToClient,
            sendToClass:sendToClass
        };

        function deferPromise(deferred, result, error) {
            if (result) {
                deferred.resolve(result);
            }
            else {
                deferred.reject(error);
            }
        }
    });
