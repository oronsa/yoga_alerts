angular.module('yogaAdmin', [
    'ui.router',
    'ngMaterial',
    'ngMessages',
    'ngResource',
    'ngAnimate',
    'ngAria',
    'ui.bootstrap',
    'ngSanitize',
    'mgcrea.ngStrap',
    'mgcrea.ngStrap.modal',
    'mgcrea.ngStrap.aside',
    'mgcrea.ngStrap.tooltip',
    'mgcrea.ngStrap.select',
    "chart.js",
    'ngSanitize'

])
    .constant('COLLECTIONS', {
    TOKEN_ACCOUNTS: 'tokenAccounts',
    USERS : 'users',
    ADMINS : 'admins',
    TOPICS : 'topics',
    ALERTS : 'alerts',
    CLASS : 'class'
})
    .constant('ROLES', {
        ADMIN: 'admin',
        GUEST: 'guest'
    })
    .run(function($rootScope, $state, $q, sessionService, authenticationService) {
        $rootScope.userSession = {};

        //regular expressions
        $rootScope.regexMail=/(^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$)/;
        $rootScope.regexToken=/(^().{8}$)/;
        $rootScope.regexPhone=/(^0\d([\d]{0,1})([-]{0,1})\d{6,}$)/;
        $rootScope.regexPassword=/(^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$)/;
        $rootScope.regexUserName=/([a-z]+[0-9]+)/;
        $rootScope.regexName=/(^([א-ת]([-, ]{0,1})([א-ת]{0,10})){1,15}$)/;
        $rootScope.regexTitle=/^.{1,50}$/;
        $rootScope.regexMessage=/^.{1,2000}$/;
        $rootScope.regexUrl =/(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
        $rootScope.regexTopic =/^[a-zA-Z0-9\-_]{1,50}$/;
        $rootScope.regexTopicHebrew =/[א-ת]+/;
        $rootScope.regexAmount = /^\d+(\.\d{1,2})?$/;


        $rootScope.createSession = function(userId, type) {
            $rootScope.userSession.userId = userId;
            $rootScope.userSession.type = type;
        };
        $rootScope.isAuthorized = function(action) {
            var deferred = $q.defer();
            sessionService.isLoggedIn().then(function(result) {
                $rootScope.createSession(result.userId,result.type);
                var userId = result.userId;
                sessionService.isAuthorized({ userId: result.userId,type: result.type, action: action}).then(function(result) {
                    if(result.isAuthorized) {
                        deferred.resolve(userId);
                    }
                    else {
                        $state.go(result.action);
                    }
                }, function (error) {
                    deferred.reject(error);
                });
            });
            return deferred.promise;
        };

        $rootScope.logOut = function() {
            authenticationService.logOut().then(function(result) {
                $state.go(result.action);
            });
        }

    })
    .config(function($compileProvider) {
        $compileProvider.preAssignBindingsEnabled(true);
    }).config(function($mdDateLocaleProvider) {
    $mdDateLocaleProvider.formatDate = function(date) {
        if(!date) {
            return '';
        }
        return moment(date).format('DD-MM-YYYY');
    }
});
