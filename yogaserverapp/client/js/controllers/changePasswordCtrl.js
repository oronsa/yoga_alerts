angular.module('yogaAdmin')


    .controller('changePasswordCtrl', function($scope, $rootScope, $state, currentUser, authenticationService) {

        $scope.data = { oldPassword : '', newPassword : '', confirmNewPassword : '' };

        $scope.submitData = function() {
            if($scope.data.newPassword != $scope.data.confirmNewPassword) {
                
            }
            else {
                authenticationService.changePassword({
                    email : $rootScope.userSession.email,
                    tempPassword : $scope.data.oldPassword,
                    password : $scope.data.newPassword
                }).then(function(result) {
                    $state.go('adminHome')
                 });
            }
        };

    });


