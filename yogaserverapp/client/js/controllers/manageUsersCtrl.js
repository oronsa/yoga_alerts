angular.module('yogaAdmin')

//
    .controller('manageUsersCtrl', function ($rootScope, $scope, $state, $stateParams,validateUtils, authenticationService, currentUser, COLLECTIONS,yogaService) {
        $scope.allUsers = [];
        $scope.allClasses = [];
        $scope.userClassArray = [];
        $scope.data = {
            date:'',
            email:'',
            newEmail:'',
            selectedUser_1:'',
            selectedUser_2:'',
            selectedUser_3:'',
            selectedUser_4:'',
            class:'',
            class_to_remove:'',
            adminToken:false
        };
        (function init() {
            yogaService.query(COLLECTIONS.USERS, {}).then(function (result) {
                if (result.collection && result.collection.length) {
                    $scope.allUsers = result.collection;
                }
            });
            yogaService.query(COLLECTIONS.CLASS, {}).then(function (result) {
                if (result.collection && result.collection.length) {
                    $scope.allClasses = result.collection;
                }
            });
        })();
        $scope.updateMembership = function() {
            $scope.data.email = $scope.data.selectedUser_1.item.email;
            yogaService.update(COLLECTIONS.USERS, {"email": $scope.data.email}, {
                $set: {
                    'membershipDate': moment($scope.data.date).format('DD/MM/YYYY')

                }
            }, {}).then(function (result) {
                swal({
                    title: "כל הכבוד !",
                    text: "המנוי עודכן בהצלחה",
                    type: "success"
                });
                $state.go($state.current, {}, {reload: true});
            });
        };
        $scope.deleteUser=function(){
            $scope.data.email = $scope.data.selectedUser_2.item.email;
            var user = _.find($scope.allUsers, function (item) {
                return item.email === $scope.data.email;
            });
            if(user && validateUtils.validatePassword($scope.data.password)){
                swal({
                        title: "שים לב ",
                        text: "אתה עומד למחוק לקוח זה יחד עם כל הפרטים שלו במערכת, האם אתה בטוח?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "כן, מחק לקוח זה",
                        cancelButtonText: "לא, ביטול ",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            authenticationService.deleteUser({adminId:currentUser,userUid:user.uid,adminPassword:$scope.data.password}).then(function(result){
                                if(result.success) {
                                    swal("המחיקה הצליחה!",
                                        "המשתמש הוסר מהמערכת בהצלחה!",
                                        "success");
                                    $state.go($state.current, {}, {reload: true});
                                }
                                else if(result.msg=='invalidPassword'){
                                    swal("אופס!",
                                        "בדוק שלא טעית בסיסמא!",
                                        "warning");
                                }
                                else{
                                    swal("אופס!",
                                        "התרחשה שגיאה, אנא נסה שנית !",
                                        "error");
                                }
                            })

                        }
                        else {
                            swal("בוטל", "המתמש נשאר במערכת",
                                "success"
                            );
                        }
                    });
            }else{
                swal({
                    title: "שים לב",
                    text: "יש למלא את 2 השדות בצורה תקינה",
                    type: "warning"
                });
            }
        };
        $scope.userToClass = function() {
            $scope.data.email = $scope.data.selectedUser_3.item.email;
            $scope.data.class = $scope.data.class.item.name;
            var user = _.find($scope.allUsers, function (item) {
                return item.email === $scope.data.email;
            });
            if (user) {
                swal({
                        title: "שים לב ",
                        text: "אתה עומד אתה עומד לבצע ציוות, האם אתה בטוח?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "כן, צוות לשיעור זה",
                        cancelButtonText: "לא, ביטול ",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            yogaService.query(COLLECTIONS.USERS,{'email': {$eq: $scope.data.email}}).then(function(result){
                                var class_name = _.find(result.collection[0].class, function (item) {
                                    return item.name === $scope.data.class;
                                });
                                if(class_name){
                                    swal("הציוות בוטל", "המשתמש כבר רשום לשיעור זה",
                                        "warning"
                                    );
                                }else{
                                    yogaService.update(COLLECTIONS.USERS,{_id:(result.collection[0]._id)},{$push:{"class":{"name":$scope.data.class}}},{}).then(function(result) {
                                        if (result.success) {
                                            swal("הציוות הצליח!",
                                                "המשתמש צוות לשיעור בהצלחה!",
                                                "success");
                                            $state.go($state.current, {}, {reload: true});
                                        }
                                        else if (result.msg == 'invalidPassword') {
                                            swal("אופס!",
                                                "בדוק שלא טעית בסיסמא!",
                                                "warning");
                                        }
                                        else {
                                            swal("אופס!",
                                                "התרחשה שגיאה, אנא נסה שנית !",
                                                "error");
                                        }
                                    });
                                }
                            });

                        }
                        else {
                            swal("בוטל", "הציוות בוטל",
                                "success"
                            );
                        }

                    });
            } else {
                swal({
                    title: "שים לב",
                    text: "יש למלא את 2 השדות בצורה תקינה",
                    type: "warning"
                });
            }
        };
        $scope.findMyClasses = function(){
            var email = $scope.data.selectedUser_4.item.email;
            yogaService.query(COLLECTIONS.USERS,{email:email}).then(function(result){

                $scope.userClassArray = result.collection[0].class;
            })
        };
        $scope.deleteFromClass = function() {
            var email = $scope.data.selectedUser_4.item.email;
            var class_name = $scope.data.class_to_remove.item.name;
            swal({
                    title: "שים לב ",
                    text: "אתה עומד אתה עומד למחוק ציוות משתמש משיעור, האם אתה בטוח?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "כן, מחק ציוות זה",
                    cancelButtonText: "לא, ביטול ",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        yogaService.update(COLLECTIONS.USERS, {email: email}, {$pull: {class:{name:class_name}}},{ multi: true }).then(function (result) {
                            if (result.success) {
                                swal("המחיקה הצליחה!",
                                    "המשתמש הוסר מהשיעור בהצלחה!",
                                    "success");
                                $state.go($state.current, {}, {reload: true});
                            } else {
                                swal("אופס!",
                                    "התרחשה שגיאה, אנא נסה שנית !",
                                    "error");
                            }
                        })
                    }
                    else {
                        swal("בוטל", "מחיקת השיעור בוטלה",
                            "success"
                        );
                    }

                });
        };
        $scope.addNewClient = function (){
            var type;
            if($scope.data.adminToken)
                type= "admin";
            else
                type = "client";

            if($scope.data.newEmail){
                {
                    authenticationService.sendToken({email:$scope.data.newEmail,type:type}).then(function(result){
                        if (result.success) {
                            swal("המפתח נוצר בהצלחה!",
                                "מפתח למשתמש זה נוצר בהצלחה!",
                                "success");
                        } else {
                            swal("אופס!",
                                "התרחשה שגיאה, אנא נסה שנית !",
                                "error");
                        }
                    });
                }

            }else{
                swal("אופס!",
                    "המייל אינו תקין, אנא נסה שנית !",
                    "error");
            }
        }
    });
