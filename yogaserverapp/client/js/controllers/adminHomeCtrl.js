angular.module('yogaAdmin')
    .controller('adminHomeCtrl', function ($rootScope,$scope, $state, currentUser, COLLECTIONS,yogaService) {

        $scope.clientList = [];

        (function init() {
                yogaService.query(COLLECTIONS.USERS, {}).then(function (result) {
                    $scope.clientList = result.collection;
                }, function (error) {
                });
        })();
    });
