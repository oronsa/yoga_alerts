angular.module('yogaAdmin')

    .controller('sendAlertsCtrl', function ($scope, $rootScope, $state,COLLECTIONS, $stateParams, authenticationService,yogaService,notificationService,validateUtils) {

        $scope.allUsers = [];
        $scope.allTopics = [];
        $scope.allClasses = [];
        $scope.data = {
            display: 'all',
            links:'noAdd',
            link: false,
            addImage:false,
            contactUs:false
        };
        $scope.sendData = {
            title: '',
            topic: '',
            message: '',
            selectedTopic: '',
            selectedUserName: '',
            selectedClass: '',
            urlPath: '',
            urlPathImage:''
        };
        $scope.sendMessage = {
            messageTitle: '',
            messageBody: ''
        };
        $scope.sendTopic = {
            topicTitle: '',
            topicTitleHebrew: ''
        };
        $scope.sendPayment = {
            paymentTitle: '',
            paymentAmount: '',
            selectedUserPayment: ''
        };
        $scope.newClass = {
            className: ''
        };
        $scope.existClass = {
            selectClassName:''
        };
        $scope.existTopic = {
            selectTopicName:''
        };
        (function init() {
            yogaService.query(COLLECTIONS.USERS, {}).then(function (result) {
                if (result.collection && result.collection.length) {
                    $scope.allUsers = result.collection;
                }
            });
            yogaService.query(COLLECTIONS.TOPICS, {}).then(function (result) {
                if (result.collection && result.collection.length) {
                    $scope.allTopics = result.collection;
                }
            });
            yogaService.query(COLLECTIONS.CLASS, {}).then(function (result) {
                if (result.collection && result.collection.length) {
                    $scope.allClasses = result.collection;
                }
            });
        })();

        $scope.submitMessage = function () {
            var dataToSend = {
                title: $scope.sendMessage.messageTitle,
                body: $scope.sendMessage.messageBody
            };
            if (!dataToSend.body || !dataToSend.title) {
                swal({
                    title: "שגיאה!",
                    text: "יש לשים לב לאורך ההודעה ואורך הנושא",
                    type: "warning"
                });
                return;
            }
            notificationService.sendToBlog(dataToSend).then(function (result) {
                if (result.success) {
                    swal("ההודעה נשלחה בהצלחה!", "כל הכבוד!", "success");
                }
                else {
                    swal({
                        title: "שגיאה!",
                        text: "משהו השתבש, נסה שנית",
                        type: "warning"
                    });
                }
            });

        };
        $scope.submitTopic = function () {
            if (validateUtils.validateTopic($scope.sendTopic.topicTitle) && validateUtils.validateTopicHebrew($scope.sendTopic.topicTitleHebrew)) {
                var sendToServer = {
                    topicTitle: $scope.sendTopic.topicTitle,
                    topicTitleHebrew: $scope.sendTopic.topicTitleHebrew
                };
                authenticationService.createNewTopic(sendToServer).then(function (result) {

                    if (result.success) {
                        swal("הנושא החדש נוצר בהצלחה!", "כל הכבוד!", "success");
                    }
                    else if (result.msg == "topicExist") {
                        swal({
                            title: "שגיאה!",
                            text: "הנושא כבר קיים, בחר נושא אחר",
                            type: "warning"
                        });
                    } else {
                        swal({
                            title: "שגיאה!",
                            text: "משהו השתבש, נסה שנית",
                            type: "warning"
                        });
                    }
                });
            }
        };
        $scope.submitData = function () {
            if ($scope.data.display === "client") {
                if ($scope.sendData.selectedUserName) {
                    $scope.sendData.email = $scope.sendData.selectedUserName.item.email;
                    if ($scope.sendData.selectedUserName) {
                        yogaService.query(COLLECTIONS.USERS, {'email': {$eq: $scope.sendData.email}}).then(function (result) {
                            if (result.collection && result.collection.length) {
                                var sendToServer = {
                                    title: $scope.sendData.title,
                                    message: $scope.sendData.message,
                                    to: result.collection[0].firebaseToken
                                };
                                if($scope.data.contactUs)
                                    sendToServer["flag"] = "contact";
                                else
                                    sendToServer["flag"] = "noContact";
                                if ($scope.data.links=='link') {
                                    var isValid = validateUtils.validateUrl($scope.sendData.urlPath);
                                    if (isValid) {
                                        sendToServer["url"] = $scope.sendData.urlPath;
                                    }
                                    else {
                                        swal({
                                            title: "שגיאה!",
                                            text: "הלינק אינו תקין, אנא נסה שנית",
                                            type: "warning"
                                        });
                                        return;
                                    }
                                    sendToServer["image"] = '';
                                }
                                else if ($scope.data.links=='image') {
                                    isValid = validateUtils.validateUrl($scope.sendData.urlPathImage);
                                    if (isValid) {
                                        sendToServer["image"] = $scope.sendData.urlPathImage;
                                    }else {
                                        swal({
                                            title: "שגיאה!",
                                            text: "הלינק לתמונה אינו תקין, אנא נסה שנית",
                                            type: "warning"
                                        });
                                        return;
                                    }
                                    sendToServer["url"] = '';
                                }else{
                                    sendToServer["url"] = '';
                                    sendToServer["image"] = '';
                                }
                                notificationService.sendToClient(sendToServer).then(
                                    function (result) {
                                        if (result.success) {
                                            swal("ההתראה נשלחה בהצלחה!", "כל הכבוד!", "success");
                                        }
                                        else {
                                            swal({
                                                title: "שגיאה!",
                                                text: "משהו השתבש, נסה שנית",
                                                type: "warning"
                                            });
                                        }
                                    }
                                );
                            }
                        });
                    }
                }
                else {
                    swal({
                        title: "שגיאה!",
                        text: "יש לבחור לקוח מהרשימה, אנא נסה שנית",
                        type: "error"
                    });
                }
            } else if ($scope.data.display === "topic") {
                if ($scope.sendData.selectedTopic.item.topicTitle) {
                    sendToServer = {
                        title: $scope.sendData.title,
                        message: $scope.sendData.message,
                        topic: $scope.sendData.selectedTopic.item.topicTitle
                    };
                    if($scope.data.contactUs)
                        sendToServer["flag"] = "contact";
                    else
                        sendToServer["flag"] = "noContact";
                    if ($scope.data.links=='link') {
                        var isValid = validateUtils.validateUrl($scope.sendData.urlPath);
                        if (isValid) {
                            sendToServer["url"] = $scope.sendData.urlPath;
                        }
                        else {
                            swal({
                                title: "שגיאה!",
                                text: "הלינק אינו תקין, אנא נסה שנית",
                                type: "warning"
                            });
                            return;
                        }
                        sendToServer["image"] = '';
                    }
                    else if ($scope.data.links=='image') {
                        isValid = validateUtils.validateUrl($scope.sendData.urlPathImage);
                        if (isValid) {
                            sendToServer["image"] = $scope.sendData.urlPathImage;
                        }else {
                            swal({
                                title: "שגיאה!",
                                text: "הלינק לתמונה אינו תקין, אנא נסה שנית",
                                type: "warning"
                            });
                            return;
                        }
                        sendToServer["url"] = '';
                    }else{
                        sendToServer["url"] = '';
                        sendToServer["image"] = '';
                    }
                    notificationService.sendToTopic(sendToServer).then(
                        function (result) {
                            if (result.success) {
                                swal("ההתראה נשלחה בהצלחה!", "כל הכבוד!", "success");
                            }
                            else {
                                swal({
                                    title: "שגיאה!",
                                    text: "משהו השתבש, נסה שנית",
                                    type: "warning"
                                });
                            }
                        }
                    );
                } else {
                    swal({
                        title: "שגיאה!",
                        text: "יש לבחור נושא מהרשימה, אנא נסה שנית",
                        type: "error"
                    });
                }
            }
            else if ($scope.data.display === "class") {
                var tokenList = [];
                var sendToServer = {
                    title: $scope.sendData.title,
                    message: $scope.sendData.message,
                    usersInClass: ''
                };
                if($scope.data.contactUs)
                    sendToServer["flag"] = "contact";
                else
                    sendToServer["flag"] = "noContact";
                if ($scope.data.links=='link') {
                    isValid = validateUtils.validateUrl($scope.sendData.urlPath);
                    if (isValid) {
                        sendToServer["url"] = $scope.sendData.urlPath;
                    }
                    else {
                        swal({
                            title: "שגיאה!",
                            text: "הלינק אינו תקין, אנא נסה שנית",
                            type: "warning"
                        });
                        return;
                    }
                    sendToServer["image"] = '';
                }
                else if ($scope.data.links=='image') {
                    isValid = validateUtils.validateUrl($scope.sendData.urlPathImage);
                    if (isValid) {
                        sendToServer["image"] = $scope.sendData.urlPathImage;
                    }else {
                        swal({
                            title: "שגיאה!",
                            text: "הלינק לתמונה אינו תקין, אנא נסה לינק אחר",
                            type: "warning"
                        });
                        return;
                    }
                    sendToServer["url"] = '';
                }else{
                    sendToServer["url"] = '';
                    sendToServer["image"] = '';
                }
                yogaService.query(COLLECTIONS.USERS, {class: {name: $scope.sendData.selectedClass.item.name}}).then(function (result) {
                    if (result.collection && result.collection.length) {
                        for (var i = 0; i < result.collection.length; i++) {
                            tokenList[i] = result.collection[i].firebaseToken;
                        }
                        sendToServer.usersInClass = tokenList;
                        notificationService.sendToClass(sendToServer).then(function (result) {
                                if (result.success) {
                                    swal("ההתראה נשלחה בהצלחה!", "כל הכבוד!", "success");
                                }
                                else {
                                    swal({
                                        title: "שגיאה!",
                                        text: "משהו השתבש, נסה שנית",
                                        type: "warning"
                                    });
                                }
                            }
                        );
                    } else {
                        swal({
                            title: "שים לב!",
                            text: "אין אף לקוח אשר רשום לשיעור זה",
                            type: "warning"
                        });
                    }

                });
            }
            else if ($scope.data.display === "all") {

                sendToServer = {
                    title: $scope.sendData.title,
                    message: $scope.sendData.message,
                    topic: "default"
                };
                if($scope.data.contactUs)
                    sendToServer["flag"] = "contact";
                else
                    sendToServer["flag"] = "noContact";
                if ($scope.data.links=='link') {
                    isValid = validateUtils.validateUrl($scope.sendData.urlPath);
                    if (isValid) {
                        sendToServer["url"] = $scope.sendData.urlPath;
                    } else {
                        swal({
                            title: "שגיאה!",
                            text: "הלינק אינו תקין, אנא נסה שנית",
                            type: "warning"
                        });
                        return;
                    }
                }
                else {
                    sendToServer["url"] = '';
                }
                if($scope.data.links=='image'){
                    isValid = validateUtils.validateUrl($scope.sendData.urlPathImage);
                    if (isValid) {
                        sendToServer["image"] = $scope.sendData.urlPathImage;
                    }else {
                        swal({
                            title: "שגיאה!",
                            text: "הלינק לתמונה אינו תקין, אנא נסה שנית",
                            type: "warning"
                        });
                        return;
                    }
                }else{
                    sendToServer["image"] = '';
                }
                notificationService.sendToTopic(sendToServer).then(
                    function (result) {
                        if (result.success) {
                            swal("ההתראה נשלחה בהצלחה!", "כל הכבוד!", "success");
                        }
                        else {
                            swal({
                                title: "שגיאה!",
                                text: "משהו השתבש, נסה שנית",
                                type: "warning"
                            });
                        }
                    }
                );
            }
        };
        $scope.sendPaymentMessage = function () {
            if (validateUtils.validateAmount($scope.sendPayment.paymentAmount)) {
                if ($scope.sendPayment.selectedUserPayment) {
                    var email = $scope.sendPayment.selectedUserPayment.item.email;
                    if (email) {
                        yogaService.query(COLLECTIONS.USERS, {'email': {$eq: email}}).then(function (result) {
                            if (result.collection && result.collection.length) {
                                var sendToServer = {
                                    title: $scope.sendPayment.paymentTitle,
                                    message: $scope.sendPayment.paymentAmount,
                                    to: result.collection[0].firebaseToken
                                };
                                notificationService.paymentToClient(sendToServer).then(
                                    function (result) {
                                        if (result.success) {
                                            swal("ההתראה נשלחה בהצלחה!", "כל הכבוד!", "success");
                                        }
                                        else {
                                            swal({
                                                title: "שגיאה!",
                                                text: "משהו השתבש, נסה שנית",
                                                type: "warning"
                                            });
                                        }
                                    }
                                );
                            }
                        });
                    }
                }
            }
        };

        $scope.createNewClass = function () {
            if (validateUtils.validateTopic($scope.newClass.className)) {
                yogaService.query(COLLECTIONS.CLASS, {name: $scope.newClass.className}).then(function (result) {
                    if (result.collection && result.collection.length) {
                        swal({
                            title: "שגיאה!",
                            text: "שיעור זה כבר קיים, צור שיעור אחר",
                            type: "warning"
                        });
                    } else {
                        yogaService.insert(COLLECTIONS.CLASS, {name: $scope.newClass.className}).then(function (result) {
                            if (result.success) {
                                swal("השיעור נוצר בהצלחה!", "כל הכבוד!", "success");
                                location.reload();
                            } else {
                                swal({
                                    title: "שגיאה!",
                                    text: "משהו השתבש, נסה שנית",
                                    type: "warning"
                                });
                            }
                        })
                    }
                })
            }

        };
        $scope.removeClass = function () {
            swal({
                    title: "שים לב ",
                    text: "אתה עומד אתה עומד למחוק שיעור, האם אתה בטוח?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "כן, מחק שיעור זה",
                    cancelButtonText: "לא, ביטול ",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        yogaService.remove(COLLECTIONS.CLASS,{name:$scope.existClass.selectClassName.item.name}).then(function(result){
                            if (result.success) {
                                swal("השיעור נמחק בהצלחה!", "כל הכבוד!", "success");
                                location.reload();
                            }
                            else {
                                swal({
                                    title: "שגיאה!",
                                    text: "משהו השתבש, נסה שנית",
                                    type: "warning"
                                });
                            }
                        })
                    }
                    else {
                        swal("בוטל", "מחיקת השיעור בוטלה",
                            "success"
                        );
                    }

                });
        };
        $scope.removeTopic = function () {
            swal({
                    title: "שים לב ",
                    text: "אתה עומד אתה עומד למחוק נושא, האם אתה בטוח?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "כן, מחק נושא זה",
                    cancelButtonText: "לא, ביטול ",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        yogaService.remove(COLLECTIONS.TOPICS,{topicTitle:$scope.existTopic.selectTopicName.item.topicTitle}).then(function(result){
                            if (result.success) {
                                swal("הנושא נמחק בהצלחה!", "כל הכבוד!", "success");
                                location.reload();
                            }
                            else {
                                swal({
                                    title: "שגיאה!",
                                    text: "משהו השתבש, נסה שנית",
                                    type: "warning"
                                });
                            }
                        })
                    }
                    else {
                        swal("בוטל", "מחיקת הנושא בוטלה",
                            "success"
                        );
                    }

                });
        }
    });