angular.module('yogaAdmin')

    .controller('autoAlertsCtrl', function ($scope, $rootScope, $state,COLLECTIONS,validateUtils, $stateParams, yogaService,notificationService) {

        $scope.mytime = new Date();
        $scope.allTopics = [];
        $scope.alertList = [];
        $scope.fromDay = ['ראשון','שני','שלישי','רביעי','חמישי','שישי','שבת'];
        $scope.toDay = ['ראשון','שני','שלישי','רביעי','חמישי','שישי','שבת'];
        $scope.hstep = 1;
        $scope.mstep = 1;
        $scope.addLink=false;

        $scope.data = {
            display:'all'
        };
        $scope.sendData = {
            title:'',
            message:'',
            selectedTopic:'',
            selectedClass:'',
            selectedFromDay:'',
            selectedToDay:'',
            urlPath:'',
            tokenList:[]
        };

        (function init() {
            yogaService.query(COLLECTIONS.TOPICS, {}).then(function (result) {
                if (result.collection && result.collection.length) {
                    $scope.allTopics = result.collection;
                }
            });
            yogaService.query(COLLECTIONS.ALERTS, {}).then(function (result) {
                if (result.collection && result.collection.length) {
                    $scope.alertList = result.collection;
                }
            });
            yogaService.query(COLLECTIONS.CLASS, {}).then(function (result) {
                if (result.collection && result.collection.length) {
                    $scope.allClasses = result.collection;
                }
            });
        })();

        $scope.clear = function() {
            $scope.sendData.mytime = null;
        };
        $scope.submitData = function(){
            if(!$scope.sendData.mytime) {
                swal({
                    title: "שגיאה!",
                    text: "יש לבחור שעה בה תוצג ההתראה",
                    type: "warning"
                });
                return;
            }
            else{
                var min,hours,selectedFromDay,selectedToDay,title,message,selectedTopic='',selectedClass='';
                if($scope.sendData.mytime.getMinutes()<10)
                    min = "0"+$scope.sendData.mytime.getMinutes();
                else
                    min = $scope.sendData.mytime.getMinutes();
                if($scope.sendData.mytime.getHours()<10)
                    hours = "0"+$scope.sendData.mytime.getHours();
                else
                    hours = $scope.sendData.mytime.getHours();
            }
            if($scope.sendData.selectedFromDay=='' || $scope.sendData.selectedToDay=='') {
                swal({
                    title: "שגיאה!",
                    text: "יש להזין יום התחלה ויום סיום, עבור יום אחד בשבוע, לדוגמא ראשון יש להזין ראשון עד ראשון",
                    type: "warning"
                });
                return;
            }
            if($scope.data.display==='class') {
                if(!$scope.sendData.selectedClass){
                    swal({
                        title: "שגיאה!",
                        text: "יש לבחור שיעור מהרשימה",
                        type: "warning"
                    });
                }
                selectedClass = $scope.sendData.selectedClass.item.name;
            }
            else if($scope.data.display==='all'){
                selectedTopic = 'default'
            }else {
                if ($scope.sendData.selectedTopic == '') {
                    swal({
                        title: "שגיאה!",
                        text: "יש לבחור קבוצת עוקבים",
                        type: "warning"
                    });
                    return;
                }else{
                    selectedTopic = $scope.sendData.selectedTopic.item.topicTitle;
                }
            }
            message = $scope.sendData.message;
            title = $scope.sendData.title;
            selectedFromDay = $scope.sendData.selectedFromDay.item;
            selectedToDay=$scope.sendData.selectedToDay.item;

            var sendToServer = {
                min:min,
                hours:hours,
                time:hours+":"+min,
                selectedFromDay: selectedFromDay,
                selectedToDay:selectedToDay,
                title: title,
                message:message,
                selectedTopic:selectedTopic,
                selectedClass:selectedClass
            };
            if ($scope.addLink) {
                var isValid = validateUtils.validateUrl($scope.sendData.urlPath);
                if (isValid) {
                    sendToServer["url"]=$scope.sendData.urlPath;
                }else {
                    swal({
                        title: "שגיאה!",
                        text: "הקישור אינו תקין, נסה שנית",
                        type: "warning"
                    });
                    return;
                }
            }else {
                sendToServer["url"] = '';
            }
            notificationService.createNewAlert(sendToServer).then(
                function (result) {
                    if (result.success) {
                        location.reload();
                    }else if(result.msg==='noSignToClass'){
                        swal({
                            title: "שים לב!",
                            text: "אין עדיין לקוחות אשר רשומים לשיעור זה, ההתראה בוטלה",
                            type: "warning"
                        });
                    }
                    else {
                        swal({   title: "שגיאה!",
                            text: "משהו השתבש, נסה שנית",
                            type: "warning"
                        });
                    }
                }
            );
        };
        $scope.deleteAlert = function(item){
            swal({
                    title: "שים לב ",
                    text: "אתה עומד אתה למחוק התראה מתוזמנת, האם אתה בטוח?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "כן, מחק התראה זו",
                    cancelButtonText: "לא, ביטול ",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if(isConfirm) {
                        notificationService.removeAlert(item.message).then(
                            function (result) {
                                if (result.success) {
                                    location.reload();
                                }
                                else {
                                    swal({
                                        title: "שגיאה!",
                                        text: "משהו השתבש, נסה שנית",
                                        type: "warning"
                                    });
                                }
                            }
                        );
                    }else{
                        swal("בוטל", "מחיקת ההתראה התבטלה",
                            "success"
                        );
                    }
                }
            )}
    });