angular.module('yogaAdmin')

//
    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/logIn');
        $stateProvider

            .state('logIn', {
                url:'/logIn',
                templateUrl: 'js/templates/logIn.html',
                controller: 'logInCtrl',
                resolve : { currentUser : function($rootScope) {
                    return $rootScope.isAuthorized(this.self.name);
                }}
            })
            .state('signUp', {
                url:'/signUp',
                templateUrl: 'js/templates/signUp.html',
                controller: 'signUpCtrl',
                resolve : { currentUser : function($rootScope) {
                    return $rootScope.isAuthorized(this.self.name);
                }}
            })
            .state('passwordRecovery', {
                url:'/passwordRecovery',
                templateUrl: 'js/templates/passwordRecovery.html',
                controller: 'passwordRecoveryCtrl',
                resolve : { currentUser : function($rootScope) {
                    return $rootScope.isAuthorized(this.self.name);
                }}
            })
            .state('authenticateRecovery', {
                url:'/authenticateRecovery',
                templateUrl: 'js/templates/authenticateRecovery.html',
                controller: 'authenticateRecoveryCtrl',
                resolve : { currentUser : function($rootScope) {
                    return $rootScope.isAuthorized(this.self.name);
                }}
            })
            .state('changePassword', {
                url:'/changePassword',
                templateUrl: 'js/templates/changePassword.html',
                controller: 'changePasswordCtrl',
                resolve : { currentUser : function($rootScope) {
                    return $rootScope.isAuthorized(this.self.name);
                }}
            })
            .state('adminHome', {
                url:'/adminHome',
                templateUrl: 'js/templates/adminHome.html',
                controller: 'adminHomeCtrl',
                resolve : { currentUser : function($rootScope) {
                    return $rootScope.isAuthorized(this.self.name);
                }}
            })
            .state('sendAlerts', {
                url:'/sendAlerts',
                templateUrl: 'js/templates/sendAlerts.html',
                controller: 'sendAlertsCtrl',
                resolve : { currentUser : function($rootScope) {
                    return $rootScope.isAuthorized(this.self.name);
                }}
            })
            .state('manageUsers', {
                url:'/manageUsers',
                templateUrl: 'js/templates/manageUsers.html',
                controller: 'manageUsersCtrl',
                resolve : { currentUser : function($rootScope) {
                    return $rootScope.isAuthorized(this.self.name);
                }}
            })
            .state('editProfile', {
                url:'/editProfile',
                templateUrl: 'js/templates/editProfile.html',
                controller: 'editProfileCtrl',
                resolve : { currentUser : function($rootScope) {
                    return $rootScope.isAuthorized(this.self.name);
                }}
            })
            .state('autoAlerts', {
                url:'/autoAlerts',
                templateUrl: 'js/templates/autoAlerts.html',
                controller: 'autoAlertsCtrl',
                resolve : { currentUser : function($rootScope) {
                    return $rootScope.isAuthorized(this.self.name);
                }}
            })
    }]);
